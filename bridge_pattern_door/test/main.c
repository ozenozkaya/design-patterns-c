/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "furniture_interface.h"
#include "template_factory.h"
#include "template_door.h"
#include "template_window.h"

#include "platform.h"



void Door_AttachToHome(furniture_ptr_t furniture_ptr)
{
    DOOR_IMPORT(Generic);
    GenericDoor_Show(furniture_ptr);
    printf("Now your home has a %s door with w=%f[cm] and h%f[cm] \r\n\r\n",
                    furniture_ptr->furniture_type_str,
                    furniture_ptr->width_in_cm,
                    furniture_ptr->height_in_cm);
}

int main()
{

    DOOR_IMPORT(Wooden);
    DOOR_IMPORT(Steel);
    DOOR_IMPORT(Generic);


    furniture_ptr_t my_wooden_door = WoodenDoor_Create(80.0,180.0);
    furniture_ptr_t my_steel_door = SteelDoor_Create(90.0,190.0);


    Door_AttachToHome(my_wooden_door);
    Door_AttachToHome(my_steel_door);

    GenericDoor_Destroy(my_wooden_door);
    GenericDoor_Destroy(my_steel_door);

    return 0;
}

/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "furniture_interface.h"
#include "template_factory.h"
#include "template_door.h"
#include "template_window.h"

#include "platform.h"

#define WOODEN


int main()
{

    FACTORY_IMPORT(Wooden);
    factory_if_ptr_t factory_if_ptr = Factory_Create(Wooden);


    {
        furniture_ptr_t my_door = factory_if_ptr->door_if_ptr->create_func(80.0,180.0);
        furniture_ptr_t my_wind = factory_if_ptr->window_if_ptr->create_func(100.0,60.0);

        factory_if_ptr->door_if_ptr->show_func(my_door);
        factory_if_ptr->window_if_ptr->show_func(my_wind);

        factory_if_ptr->door_if_ptr->destroy_func(my_door);
        factory_if_ptr->window_if_ptr->destroy_func(my_wind);
    }

    /*Here is where state pattern behavior comes*/
    FACTORY_SET_STATE(factory_if_ptr,Golden);

    {
        furniture_ptr_t my_door = factory_if_ptr->door_if_ptr->create_func(80.0,180.0);
        furniture_ptr_t my_wind = factory_if_ptr->window_if_ptr->create_func(100.0,60.0);

        factory_if_ptr->door_if_ptr->show_func(my_door);
        factory_if_ptr->window_if_ptr->show_func(my_wind);

        factory_if_ptr->door_if_ptr->destroy_func(my_door);
        factory_if_ptr->window_if_ptr->destroy_func(my_wind);
    }

    Factory_Destroy(factory_if_ptr);

    return 0;
}

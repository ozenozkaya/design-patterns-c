#ifdef __cplusplus
extern "C" {
#endif

/*
 * door_interface.h
 *
 *  Created on: 22 02 2017
 *      Author: Özen Özkaya
 */

#ifndef INC_DOOR_INTERFACE_H_
#define INC_DOOR_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door.h"

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/


typedef float (*door_get_dimension_func_t)();
typedef door_ptr_t (*create_door_func_t)(door_types_t door_type,
                float width_in_cm, float height_in_cm);
typedef void (*show_door_func_t)(door_ptr_t door_ptr);
typedef void (*destroy_door_func_t)(door_ptr_t door_ptr);

typedef struct
{
    door_get_dimension_func_t get_width_func;
    door_get_dimension_func_t get_height_func;
    create_door_func_t create_door_func;
    show_door_func_t show_door_func;
    destroy_door_func_t destroy_door_func;
}door_factory_if_t, *door_factory_if_ptr_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_DOOR_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

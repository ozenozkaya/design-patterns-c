# SIMPLE FACTORY PATTERN
Simple factory pattern is used for simply automating object creation.
For this purpose we create a factory who creates any product objects.

### src 
Includes builder pattern implementation with C language. 
### inc 
Under inc folder you will find the header files. 

### doc 
Under doc folder normally you will find the Papyrus UML model files and doxygen
configuration file.

### test 
Test folder contains a test function which demonstrates the usage of the library.
In the future, unit tests will be added.
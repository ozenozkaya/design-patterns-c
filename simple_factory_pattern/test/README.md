## SINGLETON TEST APP

### How to build
gcc -std=c90 "-ID:\\workspace.despat\\simple_factory_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -std=c90 "-ID:\\workspace.despat\\simple_factory_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\door.o" "..\\src\\door.c" 

gcc -std=c90 "-ID:\\workspace.despat\\simple_factory_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\simple_door_factory.o" "..\\src\\simple_door_factory.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o simple_factory_pattern_door.exe "src\\door.o" "src\\simple_door_factory.o" "test\\main.o" 


### Function output

Imagine a Wooden door with w=80.000000[cm] - h=180.000000[cm] 

Imagine a Golden door with w=60.000000[cm] - h=160.000000[cm] 


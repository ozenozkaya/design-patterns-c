# ABSTRACT FACTORY DESIGN PATTERN (MACRO BASED IMPLEMENATATION)
The abstract factory pattern provides a way to encapsulate a group of individual factories that have a common theme without specifying their concrete classes.["Head First Design Patterns"] In normal usage, the client software creates a concrete implementation of the abstract factory and then uses the generic interface of the factory to create the concrete objects that are part of the theme. The client doesn't know (or care) which concrete objects it gets from each of these internal factories, since it uses only the generic interfaces of their products.[1] This pattern separates the details of implementation of a set of objects from their general usage and relies on object composition, as object creation is implemented in methods exposed in the factory interface.[ "Head First Design Patterns"]

The essence of the Abstract Factory Pattern is to "Provide an interface for creating families of related or dependent objects without specifying their concrete classes. ["GoF"]

In this implementation, abstract factory pattern  is implemented with macros.
Functionality and higher level model is similar with factrory method design pattern door example, but implementation is highly different. 
Additionally, factory now can produce doors and windows with material types golden,wooden and steel.

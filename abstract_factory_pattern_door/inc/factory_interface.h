
#ifdef __cplusplus
extern "C" {
#endif

/*
 * door_interface.h
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef INC_FACTORY_INTERFACE_H_
#define INC_FACTORY_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "furniture_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

typedef struct _factory_if_t
{
    furniture_if_ptr_t window_if_ptr;
    furniture_if_ptr_t door_if_ptr;
}factory_if_t, *factory_if_ptr_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_FACTORY_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

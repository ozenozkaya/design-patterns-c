## BUILDER TEST APP

### How to build
gcc "-ID:\\workspace.despat\\abstract_factory_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o abstract_factory_pattern_door.exe "test\\main.o" 



### Function output
Imagine a Wooden door with w=80.000000[cm] - h=180.000000[cm] 

Assume you have a Wooden window with w=100.000000[cm] - h=60.000000[cm] 

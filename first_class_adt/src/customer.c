#define _CUSTOMER_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include "customer.h"
#include "platform.h"
#include "order.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/****************************************************************************/
/**                                                                        **/
/**                    PRIVATE TYPEDEFS AND STRUCTURES                     **/
/**                                                                        **/
/****************************************************************************/

/** @brief this struct is just a dummy data pack and it can be changed
 * according to application. Here we want to model a customer and we want to
 * hide the implementation details of customer. You can check @file customer.h
 * to be able to see how interface hides customer_t*/
struct customer_t
{
    const char* name; /*!< name of customer*/
    address_t address; /*!< address of customer*/
    size_t num_orders; /*!< number of orders of customer*/
    int age; /*!< age of customer*/
    order_t* orders; /*!< order list of customer*/
};

/*!**************************************************************************
 \brief Customer_Create
 -Function creates a customer and returns its reference (address in memory)

 \author Ozen Ozkaya
 @param[in]  name -Name of the customer to be created
 @param[in]  address -Address of the customer to be created
 @param[out] - customer pointer
 @return -
 @warn It is important here to return customer_ptr_t, not customer_t to keep
 entity agnostic of internal customer data structure
 *****************************************************************************/
customer_ptr_t Customer_Create(const char* name, const address_t* address)
{
    customer_ptr_t customer = PLATFORM_MALLOC(sizeof *customer);
    memset(customer, 0, sizeof * customer);
    if (customer)
    {
        /* Initialize each field in the customer... */
        customer->name = name;
        customer->address = *address;
        customer->num_orders = strlen(name);
        customer->orders = (order_t*) PLATFORM_MALLOC(sizeof(order_t) * customer->num_orders);
        memset(customer->orders, 0, sizeof * customer->orders);
        for (int i = 0; i < customer->num_orders; i++)
        {
            customer->orders[i].orderId = i;
        }
    }
    return customer;
}

/*!**************************************************************************
 \brief Customer_PrintInfo
 -Function prints some sort of information of customer.

 \author Ozen Ozkaya
 @param[in]  customer -Pointer(reference) of customer
 @return -
 @warn It is important here to pass customer_ptr_t, not customer_t to keep
 entity agnostic of internal customer data structure
 *****************************************************************************/
void Customer_PrintInfo(customer_ptr_t customer)
{
    assert(customer);
    printf("Customer Name: \t\t%s\r\n", customer->name);
    printf("Customer address: \t%s-%s\r\n", customer->address.city, customer->address.street);
    printf("Customer orders: \t%d\r\n", customer->num_orders);
}

/*!**************************************************************************
\brief Customer_Destroy
-Function destroys the costmer object instance.

\author Ozen Ozkaya
@param[in]  customer -Pointer(reference) of customer
@return -
@warn It is important here to pass customer_ptr_t, not customer_t to keep
entity agnostic of internal customer data structure
*****************************************************************************/
void Customer_Destroy(customer_ptr_t customer)
{
    /* Perform clean-up of the customer internals, if necessary. */
	memset(customer->orders, 0, sizeof * customer->orders);
    PLATFORM_FREE(customer->orders);
    customer->orders=NULL;
    memset(customer, 0, sizeof * customer);
    PLATFORM_FREE(customer);
    customer=NULL;
}

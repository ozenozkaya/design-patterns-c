#ifdef __cplusplus
extern "C" {
#endif

#ifndef ADDRESS_H
#define ADDRESS_H

/** @brief this struct is a dummy address struct. */
typedef struct _address_t
{
  const char* street;   /*!< name of street*/
  const char* city;     /*!< name of city*/
} address_t, *address_ptr_t;

#endif

#ifdef __cplusplus
}
#endif

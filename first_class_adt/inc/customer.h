#ifdef __cplusplus
extern "C" {
#endif

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include "address.h"

/* 
 A pointer to an incomplete type (hides the implementation details).
 */
typedef struct customer_t* customer_ptr_t;


customer_ptr_t Customer_Create(const char* name, const address_t* address);
void Customer_PrintInfo(customer_ptr_t customer);
void Customer_Destroy(customer_ptr_t customer);

#endif

#ifdef __cplusplus
}
#endif

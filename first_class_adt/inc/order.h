#ifdef __cplusplus
extern "C" {
#endif

#ifndef ORDER_H
#define ORDER_H

/** @brief this struct is a dummy order struct. */
typedef struct _order_t
{
  unsigned long orderId; /*!< unique id of order */
} order_t, *order_ptr_t;

#endif

#ifdef __cplusplus
}
#endif

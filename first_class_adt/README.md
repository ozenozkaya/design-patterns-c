# 1ST CLASS ABSTRACT DATA TYPE PATTERN
1st class abstract data type pattern hides internal implementation details of an object.
In this sample application, it is done in customer.h. You can see that /test/main.c 
does not know internals of customer data structure, but it can use it. 
It reduces coupling and increases cohesive behavior.

### src 
Includes builder pattern implementation with C language. 
### inc 
Under inc folder you will find the header files. 

### doc 
Under doc folder normally you will find the Papyrus UML model files which can be imported.
It is not so meaningful to draw a class diagram for this pattern. And call graph can be
automatically created with doxygen. So, there  is no sepertated UML model.

### test 
Test folder contains a test function which demonstrates the usage of the library and singleton behavior.
In the future, unit tests will be added.
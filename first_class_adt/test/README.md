## SINGLETON TEST APP

### How to build
gcc -std=c90 "-ID:\\workspace.despat\\first_class_adt\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\customer.o" "..\\src\\customer.c" 

gcc -std=c90 "-ID:\\workspace.despat\\first_class_adt\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o first_class_adt.exe "src\\customer.o" "test\\main.o" 

### Function output

Customer Name: 		ideal

Customer address: 	kartal-siemens

Customer orders: 	5
/*
 * main.c
 *
 *  Created on: 10 ?ub 2017
 *      Author: Özen Özkaya
 */
#include "customer.h"

int main()
{

	address_t addr={"siemens","kartal"};
	customer_ptr_t customerPtr=Customer_Create("ideal", &addr);


	/*Here I use the customerPtr without having
	 * a knowledge of its internals*/
	Customer_PrintInfo(customerPtr);

	Customer_Destroy(customerPtr);
	return 0;
}


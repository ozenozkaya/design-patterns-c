#define _ACCOUNT_INTERFACE_BASE_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "account_interface.h"
#include "platform.h"


/****************************************************************************/
/**                                                                        **/
/**                 PROTOTYPES OF LOCAL/PRIVATE FUNCTIONS                  **/
/**                                                                        **/
/****************************************************************************/
static void AccountBase_SetNextAccount(account_if_ptr_t a_base_account_if_ptr, account_if_ptr_t a_next_account_if_ptr);
static void AccountBase_Pay(account_if_ptr_t a_account_if_ptr, uint32_t a_amount_to_pay);
static bool AccountBase_CanPay(account_if_ptr_t a_account_if_ptr, uint32_t a_amount_to_pay);


/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/

account_if_ptr_t AccountBase_Create(uint32_t a_balance)
{
    account_if_ptr_t account_if_ptr = PLATFORM_MALLOC(sizeof *account_if_ptr);
    account_if_ptr->balance = a_balance;
    account_if_ptr->pay_func = AccountBase_Pay;
    account_if_ptr->can_pay_func= AccountBase_CanPay;
    account_if_ptr->set_next_account_func = AccountBase_SetNextAccount;
    account_if_ptr->next_account_if_ptr = NULL;
    account_if_ptr->name = "base";
    return account_if_ptr;
}

void AccountBase_Destroy(account_if_ptr_t a_account_if_ptr)
{
    if(a_account_if_ptr)
    {
        PLATFORM_FREE(a_account_if_ptr);
        a_account_if_ptr=NULL;
    }
}

/****************************************************************************/
/**                                                                        **/
/**                LOCAL/PRIVATE FUNCTION IMPLEMENTATIONS                  **/
/**                                                                        **/
/****************************************************************************/


static void AccountBase_SetNextAccount(account_if_ptr_t a_base_account_if_ptr, account_if_ptr_t a_next_account_if_ptr)
{
    a_base_account_if_ptr->next_account_if_ptr = a_next_account_if_ptr;
}

static void AccountBase_Pay(account_if_ptr_t a_account_if_ptr, uint32_t a_amount_to_pay)
{
    if(AccountBase_CanPay(a_account_if_ptr,a_amount_to_pay))
    {
        printf("Paid %d using %s.\r\n",a_amount_to_pay,a_account_if_ptr->name);
    }
    else if(a_account_if_ptr->next_account_if_ptr)
    {
        printf("Cannot pay %d using %s. Proceeding...\r\n",a_amount_to_pay,a_account_if_ptr->name);
        a_account_if_ptr->next_account_if_ptr->pay_func(a_account_if_ptr->next_account_if_ptr, a_amount_to_pay);
    }
    else
    {
        printf("Cannot pay %d because none has enough balance...\r\n",a_amount_to_pay);
    }
}

static bool AccountBase_CanPay(account_if_ptr_t a_account_if_ptr, uint32_t a_amount_to_pay)
{
    if(a_account_if_ptr->balance >= a_amount_to_pay)
    {
        return true;
    }
    return false;
}

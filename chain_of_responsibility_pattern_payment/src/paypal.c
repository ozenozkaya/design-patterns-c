#define _PAYPAL_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "account_interface.h"



/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/

account_if_ptr_t Paypal_Create(uint32_t a_balance)
{
    account_if_ptr_t account_if_ptr;
    account_if_ptr = AccountBase_Create(a_balance);
    account_if_ptr->name = "paypal";
    return account_if_ptr;
}

void Paypal_Destroy(account_if_ptr_t a_account_if_ptr)
{
    AccountBase_Destroy(a_account_if_ptr);
}


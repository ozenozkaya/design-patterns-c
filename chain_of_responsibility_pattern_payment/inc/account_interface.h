#ifdef __cplusplus
extern "C" {
#endif

/*
 * account_interface.h
 *
 *  Created on: 17 Mar 2017
 *      Author: Özen Özkaya
 */

#ifndef ACCOUNT_INTERFACE_H_
#define ACCOUNT_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

    typedef struct _account_if_t* account_if_ptr_t;
    typedef void (*set_next_account_func_t)(account_if_ptr_t base_account_if_ptr, account_if_ptr_t next_account_if_ptr);
    typedef void (*pay_func_t)(account_if_ptr_t account_if_ptr, uint32_t amount_to_pay);
    typedef bool (*can_pay_func_t)(account_if_ptr_t account_if_ptr, uint32_t amount_to_pay);

    typedef struct _account_if_t
    {
        uint32_t balance;
        char* name;

        set_next_account_func_t set_next_account_func;
        pay_func_t pay_func;
        can_pay_func_t can_pay_func;

        account_if_ptr_t next_account_if_ptr;
    }account_if_t,*account_if_ptr_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/
    account_if_ptr_t AccountBase_Create(uint32_t a_balance);
    void AccountBase_Destroy(account_if_ptr_t a_account_if_ptr);

#endif /* ACCOUNT_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

/*
 * platform.h
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif
#ifndef PLATFORM_H_
#define PLATFORM_H_

#include <stdlib.h>

/*! MALLOC mapping*/
#define PLATFORM_MALLOC    malloc

/*! CALLOC mapping*/
#define PLATFORM_CALLOC    calloc

/*! FREE mapping*/
#define PLATFORM_FREE      free


#endif /* PLATFORM_H_ */
#ifdef __cplusplus
}
#endif

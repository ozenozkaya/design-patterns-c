## COMPOSITE PATTERN TEST APP

### How to build

gcc -std=c90 "-ID:\\workspace.despat\\chain_of_responsibility\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -ansi -o "src\\paypal.o" "..\\src\\paypal.c" 

gcc -std=c90 "-ID:\\workspace.despat\\chain_of_responsibility\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -ansi -o "test\\chain_of_responsibility.o" "..\\test\\chain_of_responsibility.c" 

gcc -std=c90 "-ID:\\workspace.despat\\chain_of_responsibility\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -ansi -o "src\\account_interface_base.o" "..\\src\\account_interface_base.c" 

gcc -std=c90 "-ID:\\workspace.despat\\chain_of_responsibility\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -ansi -o "src\\bank.o" "..\\src\\bank.c" 

gcc -std=c90 "-ID:\\workspace.despat\\chain_of_responsibility\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -ansi -o "src\\bitcoin.o" "..\\src\\bitcoin.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o chain_of_responsibility.exe "src\\account_interface_base.o" "src\\bank.o" "src\\bitcoin.o" "src\\paypal.o" "test\\chain_of_responsibility.o" 



### Function output


******* TEST CASE 1 *******************

Cannot pay 120 using bank. Proceeding...

Paid 120 using paypal.



******* TEST CASE 2 *******************

Cannot pay 210 using bank. Proceeding...

Cannot pay 210 using paypal. Proceeding...

Paid 210 using bitcoin.



******* TEST CASE 3 *******************

Cannot pay 290 using bank. Proceeding...

Cannot pay 290 using paypal. Proceeding...

Paid 290 using bitcoin.



******* TEST CASE 4 *******************

Cannot pay 345 using bank. Proceeding...

Cannot pay 345 using paypal. Proceeding...

Cannot pay 345 because none has enough balance...


/*
 ============================================================================
 Name        : chain_of_responsibility.c
 Author      : Özen Özkaya
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "account_interface.h"

extern account_if_ptr_t Bank_Create(uint32_t a_balance);
extern account_if_ptr_t Paypal_Create(uint32_t a_balance);
extern account_if_ptr_t Bitcoin_Create(uint32_t a_balance);

int main(void)
{

    account_if_ptr_t bank_account_ptr = Bank_Create(100);
    account_if_ptr_t paypal_account_ptr = Paypal_Create(200);
    account_if_ptr_t bitcoin_account_ptr = Bitcoin_Create(300);

    bank_account_ptr->set_next_account_func(bank_account_ptr, paypal_account_ptr);
    paypal_account_ptr->set_next_account_func(paypal_account_ptr, bitcoin_account_ptr);

    printf("\r\n******* TEST CASE #1 *******************\r\n");
    bank_account_ptr->pay_func(bank_account_ptr, 120);
    printf("\r\n******* TEST CASE #2 *******************\r\n");
    bank_account_ptr->pay_func(bank_account_ptr, 210);
    printf("\r\n******* TEST CASE #3 *******************\r\n");
    bank_account_ptr->pay_func(bank_account_ptr, 290);
    printf("\r\n******* TEST CASE #4 *******************\r\n");
    bank_account_ptr->pay_func(bank_account_ptr, 345);

    AccountBase_Destroy(bank_account_ptr);
    AccountBase_Destroy(paypal_account_ptr);
    AccountBase_Destroy(bitcoin_account_ptr);

    return EXIT_SUCCESS;
}

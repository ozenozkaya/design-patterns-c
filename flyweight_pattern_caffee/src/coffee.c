/*
 * product.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include <coffee.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "platform.h"
#include "hashmap.h"


map_t mymap;

coffee_ptr_t Coffee_Create(char* description);

void CoffeeShop_Create()
{
    if(mymap == NULL)
    {
        mymap = hashmap_new();
    }
}


coffee_ptr_t CoffeShop_TakeOrder(char* description)
{
    coffee_ptr_t coffee_ptr = NULL;
    int status = hashmap_get(mymap,description,(any_t *)&coffee_ptr);
    if(status == MAP_OK)
    {
        printf("Getting %s from cahce\r\n",description);
        return coffee_ptr;
    }
    else
    {
        coffee_ptr = Coffee_Create(description);
        printf("New coffee type created: %s\r\n",description);
        status = hashmap_put(mymap, description, coffee_ptr);
        if(status != MAP_OK)
        {
            return NULL;
        }
    }
    return coffee_ptr;

}

void CoffeeShop_Destroy()
{
    if(mymap != NULL)
    {
        hashmap_free(mymap);
        mymap=NULL;
    }
}

coffee_ptr_t Coffee_Create(char* description)
{
    coffee_ptr_t caffee_ptr = PLATFORM_MALLOC(sizeof(coffee_t));
    caffee_ptr->description = (char*)PLATFORM_MALLOC(strlen(description)+1);
    memset(caffee_ptr->description,0,strlen(description)+1);
    strcpy(caffee_ptr->description,description);
    caffee_ptr->cost_in_tl = COFFE_COST_IN_TL + strlen(description);
    return caffee_ptr;

}

void Coffee_Destroy(coffee_ptr_t coffee_ptr)
{
    if(coffee_ptr != NULL)
    {
        if(coffee_ptr->description != NULL)
        {
            PLATFORM_FREE(coffee_ptr->description);
            coffee_ptr->description = NULL;
        }
        PLATFORM_FREE(coffee_ptr);
        coffee_ptr=NULL;
    }
}


void Coffee_ShowDecription(coffee_ptr_t coffee_ptr)
{
    printf("Desc: %s\r\n",coffee_ptr->description);
}

void Coffee_ShowCost(coffee_ptr_t coffee_ptr)
{
    printf("Cost: %d[TL]\r\n",coffee_ptr->cost_in_tl);
}

void Coffee_Show(coffee_ptr_t coffee_ptr)
{
    Coffee_ShowDecription(coffee_ptr);
    Coffee_ShowCost(coffee_ptr);
}

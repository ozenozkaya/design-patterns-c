#include <coffee.h>
#include <stdlib.h>
#include <assert.h>
#include "coffee.h"

#include "hashmap.h"
#include <stdio.h>


#define NUM_MAX_COFFE_ORDERS    10
int main(void)
{

    coffee_ptr_t coffee_list[NUM_MAX_COFFE_ORDERS]={NULL};
    uint8_t index=0;
    CoffeeShop_Create();

    coffee_list[index++] = CoffeShop_TakeOrder("latte");
    coffee_list[index++] = CoffeShop_TakeOrder("americano");
    coffee_list[index++] = CoffeShop_TakeOrder("latte");
    coffee_list[index++] = CoffeShop_TakeOrder("mocha");
    coffee_list[index++] = CoffeShop_TakeOrder("americano");
    coffee_list[index++] = CoffeShop_TakeOrder("mocha");

    while(index)
    {
        if(coffee_list[index])
        {
            CoffeeShop_Destroy(coffee_list[index]);
        }
        index--;
    }
    CoffeeShop_Destroy();

    return EXIT_SUCCESS;
}



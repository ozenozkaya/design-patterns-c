## COMPOSITE PATTERN TEST APP

### How to build

gcc -std=c90 "-ID:\\workspace.despat\\flyweight_pattern_caffee\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\coffee.o" "..\\src\\coffee.c" 

gcc -std=c90 "-ID:\\workspace.despat\\flyweight_pattern_caffee\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\hashmap.o" "..\\src\\hashmap.c" 

gcc -std=c90 "-ID:\\workspace.despat\\flyweight_pattern_caffee\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "test\\main_app.o" "..\\test\\main_app.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o flyweight_pattern_caffee.exe "src\\coffee.o" "src\\hashmap.o" "test\\main_app.o" 




### Function output (formatting problems may occur, download this readme to see the proper format)

New coffee type created: latte

New coffee type created: americano

Getting latte from cahce

New coffee type created: mocha

Getting americano from cahce

Getting mocha from cahce



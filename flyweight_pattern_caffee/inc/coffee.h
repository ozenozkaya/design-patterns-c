/*
 * product.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef COFFEE_H_
#define COFFEE_H_
#include <stdint.h>

#define COFFE_COST_IN_TL    10
#define MILK_COST_IN_TL    4
#define SUGAR_COST_IN_TL    3
#define CREME_COST_IN_TL    2


typedef struct
{
    char* description;
    uint32_t cost_in_tl;
}coffee_t, *coffee_ptr_t;


void Coffee_Show(coffee_ptr_t product_ptr);
void Coffee_Destroy(coffee_ptr_t product_ptr);

void CoffeeShop_Create();
coffee_ptr_t CoffeShop_TakeOrder(char* description);
void CoffeeShop_Destroy();


#endif /* COFFEE_H_ */

#ifdef __cplusplus
}
#endif

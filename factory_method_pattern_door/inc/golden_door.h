#ifdef __cplusplus
extern "C" {
#endif

#ifndef GOLDEN_DOOR_H_
#define GOLDEN_DOOR_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_ptr_t GoldenDoor_Create(float width_in_cm, float height_in_cm);
float GoldenDoor_GetWidthInCm(door_ptr_t door_ptr);
float GoldenDoor_GetHeightInCm(door_ptr_t door_ptr);
void GoldenDoor_Show(door_ptr_t door_ptr);
void GoldenDoor_Destroy(door_ptr_t door_ptr);

#endif /* GOLDEN_DOOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

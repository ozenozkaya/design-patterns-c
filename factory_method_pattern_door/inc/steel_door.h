#ifdef __cplusplus
extern "C" {
#endif

#ifndef STEEL_DOOR_H_
#define STEEL_DOOR_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_ptr_t SteelDoor_Create(float width_in_cm, float height_in_cm);
float SteelDoor_GetWidthInCm(door_ptr_t door_ptr);
float SteelDoor_GetHeightInCm(door_ptr_t door_ptr);
void SteelDoor_Show(door_ptr_t door_ptr);
void SteelDoor_Destroy(door_ptr_t door_ptr);

#endif /* STEEL_DOOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

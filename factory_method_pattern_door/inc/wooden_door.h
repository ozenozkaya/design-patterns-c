#ifdef __cplusplus
extern "C" {
#endif

#ifndef WOODEN_DOOR_H_
#define WOODEN_DOOR_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_ptr_t WoodenDoor_Create(float width_in_cm, float height_in_cm);
float WoodenDoor_GetWidthInCm(door_ptr_t door_ptr);
float WoodenDoor_GetHeightInCm(door_ptr_t door_ptr);
void WoodenDoor_Show(door_ptr_t door_ptr);
void WoodenDoor_Destroy(door_ptr_t door_ptr);

#endif /* WOODEN_DOOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

#ifdef __cplusplus
extern "C" {
#endif


#ifndef DOOR_INTERFACE_H_
#define DOOR_INTERFACE_H_

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/
typedef enum
{
 WOODEN_DOOR,
 STEEL_DOOR,
 GOLDEN_DOOR
}door_types_t;

typedef struct
{
    float width_in_cm;
    float height_in_cm;
    door_types_t door_type;
}door_t,*door_ptr_t;

typedef door_ptr_t (*door_create_func_t)(float width_in_cm, float height_in_cm);
typedef float (*door_get_width_func_t)(door_ptr_t door_ptr);
typedef float (*door_get_height_func_t)(door_ptr_t door_ptr);
typedef void (*door_show_func_t)(door_ptr_t door_ptr);
typedef void (*door_destroy_func_t)(door_ptr_t door_ptr);

typedef struct
{
    door_create_func_t door_create_func;
    door_get_width_func_t   door_get_width_func;
    door_get_height_func_t door_get_height_func;
    door_show_func_t    door_show_func;
    door_destroy_func_t door_destroy_func;
}door_if_t,*door_if_ptr_t;
 

#endif /* DOOR_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

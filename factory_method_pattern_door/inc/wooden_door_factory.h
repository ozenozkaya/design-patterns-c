#ifdef __cplusplus
extern "C" {
#endif

#ifndef WOODEN_DOOR_FACTORY_H_
#define WOODEN_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_factory_if_ptr_t DoorFactory_CreateWoodenFactory();
void DoorFactory_DestroyWoodenFactory(door_factory_if_ptr_t door_factory_if_ptr);

#endif /* WOODEN_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

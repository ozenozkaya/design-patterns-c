#ifdef __cplusplus
extern "C" {
#endif

#ifndef SILVER_DOOR_FACTORY_H_
#define SILVER_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_factory_if_ptr_t DoorFactory_CreateSteelFactory();
void DoorFactory_DestroySteelFactory(door_factory_if_ptr_t door_factory_if_ptr);

#endif /* SILVER_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

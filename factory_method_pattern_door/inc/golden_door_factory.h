#ifdef __cplusplus
extern "C" {
#endif

#ifndef GOLDEN_DOOR_FACTORY_H_
#define GOLDEN_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_factory_if_ptr_t DoorFactory_CreateGoldenFactory();
void DoorFactory_DestroyGoldenFactory(door_factory_if_ptr_t door_factory_if_ptr);

#endif /* GOLDEN_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

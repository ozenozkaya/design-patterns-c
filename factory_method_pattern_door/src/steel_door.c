#define _DOOR_C
/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include <door_interface.h>
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
  
door_ptr_t SteelDoor_Create(float width_in_cm, float height_in_cm)
{
    door_ptr_t door_ptr = PLATFORM_MALLOC(sizeof *door_ptr);
    door_ptr->door_type = STEEL_DOOR;
    door_ptr->width_in_cm = width_in_cm;
    door_ptr->height_in_cm = height_in_cm;
    /*Some more creation logic here*/
    return door_ptr;
}

float SteelDoor_GetWidthInCm(door_ptr_t door_ptr)
{
    return door_ptr->width_in_cm;
}

float SteelDoor_GetHeightInCm(door_ptr_t door_ptr)
{
    return door_ptr->height_in_cm;
}

void SteelDoor_Show(door_ptr_t door_ptr)
{
    printf("Imagine a Steel door with w=%f[cm] - h=%f[cm] \r\n",
                    door_ptr->width_in_cm, door_ptr->height_in_cm);
}

void SteelDoor_Destroy(door_ptr_t door_ptr)
{
    PLATFORM_FREE(door_ptr);
    door_ptr = NULL;
}
 


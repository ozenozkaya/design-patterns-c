#define _SIMPLE_DOOR_FACTORY_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "steel_door_factory.h"
#include "steel_door.h"
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
door_factory_if_ptr_t DoorFactory_CreateSteelFactory(door_if_ptr_t door_if_ptr)
{
    door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);
    door_factory_if_ptr->door_if_ptr = (door_if_ptr_t)PLATFORM_MALLOC(sizeof *door_factory_if_ptr->door_if_ptr);
    door_factory_if_ptr->door_if_ptr->door_create_func = SteelDoor_Create;
    door_factory_if_ptr->door_if_ptr->door_destroy_func = SteelDoor_Destroy;
    door_factory_if_ptr->door_if_ptr->door_get_height_func = SteelDoor_GetHeightInCm;
    door_factory_if_ptr->door_if_ptr->door_get_width_func = SteelDoor_GetWidthInCm;
    door_factory_if_ptr->door_if_ptr->door_create_func = SteelDoor_Create;
    return door_factory_if_ptr;
}

void DoorFactory_DestroySteelFactory(door_factory_if_ptr_t door_factory_if_ptr)
{
    PLATFORM_FREE(door_factory_if_ptr->door_if_ptr);
    PLATFORM_FREE(door_factory_if_ptr);
    door_factory_if_ptr = NULL;
}
 

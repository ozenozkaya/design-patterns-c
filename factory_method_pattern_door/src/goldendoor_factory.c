#define _SIMPLE_DOOR_FACTORY_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "golden_door_factory.h"
#include "golden_door.h"
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
door_factory_if_ptr_t DoorFactory_CreateGoldenFactory(door_if_ptr_t door_if_ptr)
{
    door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);
    door_factory_if_ptr->door_if_ptr = (door_if_ptr_t)PLATFORM_MALLOC(sizeof *door_factory_if_ptr->door_if_ptr);
    door_factory_if_ptr->door_if_ptr->door_create_func = GoldenDoor_Create;
    door_factory_if_ptr->door_if_ptr->door_destroy_func = GoldenDoor_Destroy;
    door_factory_if_ptr->door_if_ptr->door_get_height_func = GoldenDoor_GetHeightInCm;
    door_factory_if_ptr->door_if_ptr->door_show_func = GoldenDoor_Show;
    door_factory_if_ptr->door_if_ptr->door_get_width_func = GoldenDoor_GetWidthInCm;
    return door_factory_if_ptr;
}

void DoorFactory_DestroyGoldenFactory(door_factory_if_ptr_t door_factory_if_ptr)
{
    PLATFORM_FREE(door_factory_if_ptr->door_if_ptr);
    PLATFORM_FREE(door_factory_if_ptr);
    door_factory_if_ptr = NULL;
}
 

#define _DOOR_C
/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include <door_interface.h>
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
  
door_ptr_t WoodenDoor_Create(float width_in_cm, float height_in_cm)
{
    door_ptr_t door_ptr = PLATFORM_MALLOC(sizeof *door_ptr);
    door_ptr->door_type = WOODEN_DOOR;
    door_ptr->width_in_cm = width_in_cm;
    door_ptr->height_in_cm = height_in_cm;
    /*Some more creation logic here*/
    return door_ptr;
}

float WoodenDoor_GetWidthInCm(door_ptr_t door_ptr)
{
    return door_ptr->width_in_cm;
}

float WoodenDoor_GetHeightInCm(door_ptr_t door_ptr)
{
    return door_ptr->height_in_cm;
}

void WoodenDoor_Show(door_ptr_t door_ptr)
{
    printf("Imagine a Wooden door with w=%f[cm] - h=%f[cm] \r\n",
                    door_ptr->width_in_cm, door_ptr->height_in_cm);
}

void WoodenDoor_Destroy(door_ptr_t door_ptr)
{
    PLATFORM_FREE(door_ptr);
    door_ptr = NULL;
}
 


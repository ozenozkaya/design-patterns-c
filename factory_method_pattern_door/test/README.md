## BUILDER TEST APP

### How to build
gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\woodendoor_factory.o" "..\\src\\woodendoor_factory.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\steel_door.o" "..\\src\\steel_door.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\golden_door.o" "..\\src\\golden_door.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\steeldoor_factory.o" "..\\src\\steeldoor_factory.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\wooden_door.o" "..\\src\\wooden_door.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\goldendoor_factory.o" "..\\src\\goldendoor_factory.c" 

gcc -std=c90 "-ID:\\workspace.despat\\factory_method_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o factory_method_pattern_door.exe "src\\golden_door.o" "src\\goldendoor_factory.o" "src\\steel_door.o" "src\\steeldoor_factory.o" "src\\wooden_door.o" "src\\woodendoor_factory.o" "test\\main.o" 



### Function output
Imagine a Wooden door with w=80.000000[cm] - h=180.000000[cm] 

Imagine a Golden door with w=60.000000[cm] - h=160.000000[cm] 


/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <wooden_door_factory.h>
#include <golden_door_factory.h>


int main()
{
    door_factory_if_ptr_t wooden_door_factory_if_ptr = DoorFactory_CreateWoodenFactory();
    door_factory_if_ptr_t golden_door_factory_if_ptr = DoorFactory_CreateGoldenFactory();

    door_ptr_t my_wooden_door = wooden_door_factory_if_ptr->door_if_ptr->door_create_func(80.0,180.0);
    door_ptr_t my_golden_door = golden_door_factory_if_ptr->door_if_ptr->door_create_func(60.0,160.0);

    wooden_door_factory_if_ptr->door_if_ptr->door_show_func(my_wooden_door);
    golden_door_factory_if_ptr->door_if_ptr->door_show_func(my_golden_door);

    wooden_door_factory_if_ptr->door_if_ptr->door_destroy_func(my_wooden_door);
    golden_door_factory_if_ptr->door_if_ptr->door_destroy_func(my_golden_door);

    DoorFactory_DestroyWoodenFactory(wooden_door_factory_if_ptr);
    DoorFactory_DestroyGoldenFactory(golden_door_factory_if_ptr);

    return 0;
}

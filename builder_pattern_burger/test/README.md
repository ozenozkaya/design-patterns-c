## BUILDER TEST APP

### How to build
gcc -std=c90 "-ID:\\workspace.despat\\builder_pat_burger\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\burgerking.o" "..\\src\\burgerking.c" 

gcc -std=c90 "-ID:\\workspace.despat\\builder_pat_burger\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\director.o" "..\\src\\director.c" 

gcc -std=c90 "-ID:\\workspace.despat\\builder_pat_burger\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\mcdonalds.o" "..\\src\\mcdonalds.c" 

gcc -std=c90 "-ID:\\workspace.despat\\builder_pat_burger\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "test\\main_app.o" "..\\test\\main_app.c" 

gcc -std=c90 "-ID:\\workspace.despat\\builder_pat_burger\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\burger.o" "..\\src\\burger.c" 

gcc -o builder_pat_burger.exe "src\\burger.o" "src\\burgerking.o" "src\\director.o" "src\\mcdonalds.o" "test\\main_app.o" 


### Function output (corrupted because code.siemens.com font)



~BurgerKing bke-Burger~

/|||||||||||||||||||\	 bugday_ekmegi

ooooooooooooooooooooo	 ketcap

:::::::::::::::::::::	 et 

\|||||||||||||||||||/	 bugday_ekmegi





~BurgerKing kmt-Burger~

/|||||||||||||||||||\	 kepek_ekmegi

ooooooooooooooooooooo	 mayonez

:::::::::::::::::::::	 tavuk 

\|||||||||||||||||||/	 kepek_ekmegi





~McDonalds mbe-Burger~

/|||||||||||||||||||\	 misir_ekmegi

ooooooooooooooooooooo	 bbq

:::::::::::::::::::::	 et 

\|||||||||||||||||||/	 misir_ekmegi





~McDonalds chh-Burger~

/|||||||||||||||||||\	 cavdar_ekmegi

ooooooooooooooooooooo	 hardal

:::::::::::::::::::::	 hindi 

\|||||||||||||||||||/	 cavdar_ekmegi




/*
 * builder_interface.h
 *
 *  Created on: 17 02 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_
/** Builder interface is dependent to product.
 * For different types of products, you need
 * different builder interfaces.*/
#include "burger_interface.h"

void setBuilder(builder_if_ptr_t new_builder_if_ptr);
burger_ptr_t construct(char* bread, char* sauce, char* meat);

#endif /* _DIRECTOR_H */

#ifdef __cplusplus
}
#endif

/*
 * builder_interface.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef BURGER_INTERFACE_H_
#define BURGER_INTERFACE_H_
#include "burger.h"

typedef void (*put_if_func_t)(burger_ptr_t burger_ptr, char* stuff);
typedef burger_ptr_t (*product_create_func_t)();

typedef struct
{
    put_if_func_t put_bread_func;
    put_if_func_t put_sauce_func;
    put_if_func_t put_meat_func;
    product_create_func_t burger_create_func;
}builder_if_t,*builder_if_ptr_t;


#endif /* BURGER_INTERFACE_H_ */

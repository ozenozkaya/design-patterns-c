/*
 * builder.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef BUILDERY_H_
#define BUILDERY_H_

/** This buildery module actually implements the builder interface.
 * Hence, we need relevant type definitions*/
#include "builder_interface.h"

builder_if_ptr_t BuilderY_Create();
void BuilderY_Destroy(builder_if_ptr_t builder_ptr);

#endif /* BUILDERX_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
#endif


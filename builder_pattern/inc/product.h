/*
 * product.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef PRODUCT_H_
#define PRODUCT_H_

typedef struct
{
    char* builder;
    char* part1;
    char* part2;
}product_t, *product_ptr_t;

product_ptr_t Product_Create(char* builder,char* part1, char* part2);
void Product_Show(product_ptr_t product_ptr);
void Product_Destroy(product_ptr_t product_ptr);


#endif /* PRODUCT_H_ */

#ifdef __cplusplus
}
#endif

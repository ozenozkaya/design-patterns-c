/*
 * builder_interface.h
 *
 *  Created on: 17 02 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_
/** Builder interface is dependent to product.
 * For different types of products, you need
 * different builder interfaces.*/
#include "builder_interface.h"

void setBuilder(builder_if_ptr_t new_builder_if_ptr);
product_ptr_t construct(char* part1, char* part2);

#endif /* _DIRECTOR_H */

#ifdef __cplusplus
}
#endif

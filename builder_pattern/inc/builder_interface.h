/*
 * builder_interface.h
 *
 *  Created on: 17 02 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif
#ifndef BUILDER_INTERFACE_H_
#define BUILDER_INTERFACE_H_

/** Builder interface is dependent to product.
 * For different types of products, you need
 * different builder interfaces.*/
#include "product.h"

/** put_if_func_t is a function pointer type which can be used
 * to provide an interface to build the product.
 * As you know, C language does not have a feature of virtual functions.
 * But if you provide the interface for a function but provide no
 * implementation, it works like (a degenerated approach) a virtual
 * function.
 *
 * product_create_func_t is also a function pointer type which can be used
 * create a */
typedef void (*put_if_func_t)(product_ptr_t product, char* part);
typedef product_ptr_t (*product_create_func_t)();

/*builder_if_t is a struct which contains interface functions to
 * build the product step by step. This struct contains so-called virtual
 * interfaces for builders.
 * */
typedef struct
{
    put_if_func_t build_part1_func;
    put_if_func_t build_part2_func;
    product_create_func_t product_create_func;
}builder_if_t,*builder_if_ptr_t;


#endif /* BUILDER_INTERFACE_H_ */
#ifdef __cplusplus
} // closing brace for extern "C"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BUILDER_PLATFORM_H_
#define BUILDER_PLATFORM_H_

/*! MALLOC mapping*/
#define BUILDER_MALLOC    malloc

/*! CALLOC mapping*/
#define BUILDER_CALLOC    calloc

/*! FREE mapping*/
#define BUILDER_FREE      free

#endif /* BUILDER_PLATFORM_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"

#endif

/*
 * builder.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef BUILDERX_H_
#define BUILDERX_H_

/** This builderx module actually implements the builder interface.
 * Hence, we need relevant type definitions*/
#include "builder_interface.h"

builder_if_ptr_t BuilderX_Create();
void BuilderX_Destroy(builder_if_ptr_t builder_ptr);

#endif /* BUILDERX_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
#endif

#define _BUILDER_Y_C
/** \file buildery.c
 * \brief   This module contains (one of) the builder implementation of
 *          builder interface.
 * \author  Ozen Ozkaya
 * \date    21.02.2017
*/
#include <stddef.h>
#include <stdlib.h>
#include "buildery.h"
#include "builder_platform.h"

static void BuilderY_BuildPart1(product_ptr_t product_ptr , char* part);
static void BuilderY_BuildPart2(product_ptr_t product_ptr , char* part);
static product_ptr_t BuilderY_ProductCreate();


/*!**************************************************************************
\brief BuilderY_Create
-Function creates the builder y object.

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] - pointer to singleton object
@return -
@todo: Static memory allocation option may be added.
*****************************************************************************/
builder_if_ptr_t BuilderY_Create()
{
    builder_if_ptr_t builder_ptr = BUILDER_MALLOC(sizeof(builder_if_t));
    builder_ptr->build_part1_func = BuilderY_BuildPart1;
    builder_ptr->build_part2_func = BuilderY_BuildPart2;
    builder_ptr->product_create_func = BuilderY_ProductCreate;
    return builder_ptr;
}


/*!**************************************************************************
\brief BuilderY_ProductCreate
-Function creates the a product of buildery.
The parameter list can be changed according to product.

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] -
@return - pointer to product
@warn - When builder creates a product, it must be (later) destroyed to prevent
memory leakege.
*****************************************************************************/
static product_ptr_t BuilderY_ProductCreate()
{
    return (product_ptr_t)Product_Create("buildery",NULL,NULL);
}


/*!**************************************************************************
\brief BuilderY_Destroy
-Function destroys the buildery object by freeing its memory.
\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - pointer to builder
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void BuilderY_Destroy(builder_if_ptr_t builder_ptr)
{
    BUILDER_FREE(builder_ptr);
    builder_ptr=NULL;
}


/*!**************************************************************************
\brief BuilderY_BuildPart1
-Function builds the part1 of product. It can be changed according to
implementation needs. It is just a dummy implementation.
\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - pointer to product, part1
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
static void BuilderY_BuildPart1(product_ptr_t product_ptr , char* part)
{
    product_ptr->part1 = part;
}


/*!**************************************************************************
\brief BuilderY_BuildPart2
-Function builds the part2 of product. It can be changed according to
implementation needs. It is just a dummy implementation.
\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - pointer to product, part2
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
static void BuilderY_BuildPart2(product_ptr_t product_ptr , char* part)
{
    product_ptr->part2 = part;
}




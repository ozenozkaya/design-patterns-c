#define _BUILDER_X_C
/** \file builderx.c
 * \brief   This module contains (one of) the builder implementation of
 *          builder interface.
 * \author  Ozen Ozkaya
 * \date    21.02.2017
*/
#include <stddef.h>
#include <stdlib.h>
#include "builderx.h"
#include "builder_platform.h"

static void BuilderX_BuildPart1(product_ptr_t product_ptr , char* part);
static void BuilderX_BuildPart2(product_ptr_t product_ptr , char* part);
static product_ptr_t BuilderX_ProductCreate();


/*!**************************************************************************
\brief BuilderX_Create
-Function creates the builder x object.

\author Ozen Ozkaya
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] - pointer to singleton object
@return -
@todo: Static memory allocation option may be added.
*****************************************************************************/
builder_if_ptr_t BuilderX_Create()
{
    builder_if_ptr_t builder_ptr = BUILDER_MALLOC(sizeof(builder_if_t));
    builder_ptr->build_part1_func = BuilderX_BuildPart1;
    builder_ptr->build_part2_func = BuilderX_BuildPart2;
    builder_ptr->product_create_func = BuilderX_ProductCreate;
    return builder_ptr;
}


/*!**************************************************************************
\brief BuilderX_ProductCreate
-Function creates the a product of builderx.
The parameter list can be changed according to product.

\author Ozen Ozkaya
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] -
@return - pointer to product
@warn - When builder creates a product, it must be (later) destroyed to prevent
memory leakege.
*****************************************************************************/
static product_ptr_t BuilderX_ProductCreate()
{
    return (product_ptr_t)Product_Create("builderx",NULL,NULL);
}


/*!**************************************************************************
\brief BuilderX_Destroy
-Function destroys the builderx object by freeing its memory.
\author Ozen Ozkaya
\date 20.02.2017
@param[in] - pointer to builder
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void BuilderX_Destroy(builder_if_ptr_t builder_ptr)
{
    BUILDER_FREE(builder_ptr);
    builder_ptr=NULL;
}


/*!**************************************************************************
\brief BuilderX_BuildPart1
-Function builds the part1 of product. It can be changed according to
implementation needs. It is just a dummy implementation.
\author Ozen Ozkaya
\date 20.02.2017
@param[in] - pointer to product, part1
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
static void BuilderX_BuildPart1(product_ptr_t product_ptr , char* part)
{
    product_ptr->part1 = part;
}


/*!**************************************************************************
\brief BuilderX_BuildPart2
-Function builds the part2 of product. It can be changed according to
implementation needs. It is just a dummy implementation.
\author Ozen Ozkaya
\date 20.02.2017
@param[in] - pointer to product, part2
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
static void BuilderX_BuildPart2(product_ptr_t product_ptr , char* part)
{
    product_ptr->part2 = part;
}




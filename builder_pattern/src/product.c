/*
 * product.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "product.h"

product_ptr_t Product_Create(char* builder,char* part1, char* part2)
{
    product_ptr_t product_ptr = malloc(sizeof(product_t));
    product_ptr->builder = builder;
    product_ptr->part1 = part1;
    product_ptr->part2 = part2;
    return product_ptr;

}

void Product_Destroy(product_ptr_t product_ptr)
{
    if(product_ptr != NULL)
    {
        free(product_ptr);
        product_ptr=NULL;
    }
}


void Product_Show(product_ptr_t product_ptr)
{
    printf("$$PRODUCTOF:%s|%s:%s|##\r\n",product_ptr->builder,product_ptr->part1, product_ptr->part2);
}


/*
 ============================================================================
 Name        : builder_pat.c
 Author      : Özen Özkaya
 Version     :
 Copyright   : No rights reserved
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "director.h"

/** Static, builder interface pointer to keep builder info*/
static builder_if_ptr_t builder_if_ptr;


/*!**************************************************************************
\brief setBuilder
-Function selects the builder by setting the statically global builder if
pointer to the function argument builder ptr.
\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - pointer to builder interface (generic)
@param[in,out] -
@param[out] - pointer to realted product
@return -
*****************************************************************************/
void setBuilder(builder_if_ptr_t new_builder_if_ptr)
{
    builder_if_ptr = new_builder_if_ptr;
}


/*!**************************************************************************
\brief construct
- Function constructs a product in an agnostic structure of concrete builder.
Build process is realized step by step.
\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - pointer to builder interface (generic)
@param[in,out] -
@param[out] - pointer to realted product
@return -
*****************************************************************************/
product_ptr_t construct(char* part1, char* part2)
{
    product_ptr_t product_ptr;
    product_ptr = builder_if_ptr->product_create_func();
    builder_if_ptr->build_part1_func(product_ptr, part1);
    builder_if_ptr->build_part2_func(product_ptr, part2);
    return product_ptr;
}

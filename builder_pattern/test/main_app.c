#include "director.h"
#include "builderx.h"
#include "buildery.h"

#include <stdlib.h>
/*
 * main_app.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */


int main(void) {


    builder_if_ptr_t builderx_ptr = BuilderX_Create();
    builder_if_ptr_t buildery_ptr = BuilderY_Create();
    product_ptr_t   product_ptr_A,product_ptr_B,product_ptr_C,product_ptr_D;

    setBuilder(builderx_ptr);
    product_ptr_A = construct("len","5");
    product_ptr_B = construct("width","9");

    setBuilder(buildery_ptr);
    product_ptr_C = construct("name","Bjarne");
    product_ptr_D = construct("surname","Stroup");

    Product_Show(product_ptr_A);
    Product_Show(product_ptr_B);
    Product_Show(product_ptr_C);
    Product_Show(product_ptr_D);

    Product_Destroy(product_ptr_A);
    Product_Destroy(product_ptr_B);
    Product_Destroy(product_ptr_C);
    Product_Destroy(product_ptr_D);

    BuilderX_Destroy(builderx_ptr);
    BuilderY_Destroy(buildery_ptr);

    return EXIT_SUCCESS;
}



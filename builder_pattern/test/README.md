## BUILDER TEST APP

### How to build
gcc "-ID:\\workspace.despat\\builder_pat\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\director.o" "..\\src\\director.c" 

gcc "-ID:\\workspace.despat\\builder_pat\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\product.o" "..\\src\\product.c" 

gcc "-ID:\\workspace.despat\\builder_pat\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\buildery.o" "..\\src\\buildery.c" 

gcc "-ID:\\workspace.despat\\builder_pat\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "src\\builderx.o" "..\\src\\builderx.c" 

gcc "-ID:\\workspace.despat\\builder_pat\\inc" -O0 -g3 -Wall -c -fmessage-length=0 -o "test\\main_app.o" "..\\test\\main_app.c" 

gcc -o builder_pat.exe "src\\builderx.o" "src\\buildery.o" "src\\director.o" "src\\product.o" "test\\main_app.o" 

### Function output
$$PRODUCTOF:builderx|len:5|##

$$PRODUCTOF:builderx|width:9|##

$$PRODUCTOF:buildery|name:Bjarne|##

$$PRODUCTOF:buildery|surname:Stroup|##

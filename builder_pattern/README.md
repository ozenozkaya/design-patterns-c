# BUILDER DESIGN PATTERN
The builder pattern is a software design pattern that consturcts product objects step by step with 
different type of builders which use the same interface for building.

### src 
Includes builder pattern implementation with C language. In the lowest layer you'll find product.c.
This product implementation can be fully changed according to needs (consider product.h also).
BuilderX and BuilderY are two sample builders (or builder candidates) which builds the product.
Builder implementation highly relays on builder_interface.h. Same story is valid for BuilderY.
Director is the one who uses different builders to build different products. As definition of 
builder design pattern, it only selects the builder and step by step constructs the product.

### inc 
Under inc folder you will find the header files. product.h includes interface for product.
Builder interface provides the abstract interface for builders. 

### doc 
Under doc folder you will find the Papyrus UML model files which can be imported. And exported PNG file which shows
the UML model. If you want to improve or modify the UML model, you can set up Eclypse Papyrus and enjoy it.
Doc folder also includes the doxyconfig file, with Eclox pulign of Eclipse, you can directly generate doxygen documentation.

### test 
Test folder contains a test function which demonstrates the usage of the library and singleton behavior.
In the future, unit tests will be added.
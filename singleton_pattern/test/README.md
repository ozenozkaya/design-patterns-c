## SINGLETON TEST APP

### How to build
gcc -std=c90 "-ID:\\workspace.despat\\singleton_pattern\\inc" -O0 -g3 -p -pg -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "test\\main_app.o" "..\\test\\main_app.c" 
gcc -std=c90 "-ID:\\workspace.despat\\singleton_pattern\\inc" -O0 -g3 -p -pg -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\singleton.o" "..\\src\\singleton.c" 
gcc -p -pg -o singleton_pattern.exe "src\\singleton.o" "test\\main_app.o" 

### Function output
Address of instance1=0x3b2130,Address of instance2=0x3b2130

Value of instance2 is 5

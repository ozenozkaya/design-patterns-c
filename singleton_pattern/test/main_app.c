/*
 ============================================================================
 Name        : singleton_pat.c
 Author      : 
 Version     :
 Copyright   : No rights reserved
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "singleton.h"

int main(void) {
	singleton_ptr_t instance1 = Singleton_Create();
	singleton_ptr_t instance2 = Singleton_Create();

	printf("Address of instance1=0x%x,Address of instance2=0x%x\r\n",(unsigned int)instance1,(unsigned int)instance2);

	Singleton_SetValue(instance1, 5);
	printf("Value of instance2 is %d\r\n", Singleton_GetValue(instance2));

	Singleton_Remove(instance1);
	return EXIT_SUCCESS;
}

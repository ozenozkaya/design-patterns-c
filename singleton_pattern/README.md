# SINGLETON DESIGN PATTERN
The singleton pattern is a software design pattern that restricts the instantiation of a class to one object.
This is useful when exactly one object is needed to coordinate actions across the system.
The concept is sometimes generalized to systems that operate more efficiently when only one object exists,
 or that restrict the instantiation to a certain number of objects. 
The term comes from the mathematical concept of a singleton.

### src 
Includes singleton pattern implementation with C language.

### inc 
Under inc folder you will find the header files. singleton.h file is used as library header. 
Furthermore, singleton_platform.h folder includes dynamic memory definitions which may vary from system to system.
If you want to use static or dynamic memory allocation behavior, you can select it in singleton.h file.

### doc 
Under doc folder you will find the Papyrus UML model files which can be imported. And exported PNG file which shows
the UML model. If you want to improve or modify the UML model, you can set up Eclypse Papyrus and enjoy it.
Doc folder also includes the doxyconfig file, with Eclox pulign of Eclipse, you can directly generate doxygen documentation.

### test 
Test folder contains a test function which demonstrates the usage of the library and singleton behavior.
In the future, unit tests will be added.
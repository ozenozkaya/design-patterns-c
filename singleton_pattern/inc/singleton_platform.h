/*
 * singleton_platform.h
 *
 *  Created on: 16 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef SINGLETON_PLATFORM_H_
#define SINGLETON_PLATFORM_H_

/*! MALLOC mapping*/
#define SINGLETON_MALLOC    malloc

/*! CALLOC mapping*/
#define SINGLETON_CALLOC    calloc

/*! FREE mapping*/
#define SINGLETON_FREE      free

#endif /* SINGLETON_PLATFORM_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"

#endif

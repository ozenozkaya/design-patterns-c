/*
 * singleton.h
 *
 *  Created on: 16 ?ub 2017
 *      Author: Özen Özkaya
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef SINGLETON_H_
#define SINGLETON_H_
#include "singleton_platform.h"

/** First class ADT pattern singleton instance pointer type*/
typedef struct singleton_t* singleton_ptr_t;


/** @defgroup MEMORY_ALLOC options
 *
 *  @{
 *  */
/** Static memory allocation uses a static variable for
 * singleton object storage*/
/*#define STATIC_MEMORY_ALLOC*/
#ifndef STATIC_MEMORY_ALLOC
/** Dynamic memory allocation uses a dynamic allocation
 * for singleton object storage. @see singleton_platform.h
 * to modify the platform layer bindings */
#define DYNAMIC_MEMORY_ALLOC
#endif

singleton_ptr_t Singleton_Create();
singleton_ptr_t Singleton_GetInstance();
void Singleton_Remove(singleton_ptr_t singleton_ptr);

void Singleton_SetPayload(singleton_ptr_t a_singleton_ptr, void* a_payload);
void* Singleton_GetPayload( const singleton_ptr_t a_singleton_ptr);
void Singleton_SetValue(singleton_ptr_t a_singleton_ptr, int a_value);
int Singleton_GetValue(const singleton_ptr_t a_singleton_ptr);

#endif /* SINGLETON_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"

#endif

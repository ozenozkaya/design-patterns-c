#define _SINGLETON_C
/*! \mainpage SINGLETON module
 *
 * \section intro_sec Introduction
 *
 * This documentation is written for SINGLETON pattern implementation in C.
 *
 */

/** \file singleton.c
 * \brief This module contains the SINGLETON pattern implementation in C�
 * \author Ozen Ozkaya, 
 * \date 20.02.2017
*/

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include "singleton.h"
#include <stddef.h>
#include <stdlib.h>


/****************************************************************************/
/**                                                                        **/
/**                    PRIVATE TYPEDEFS AND STRUCTURES                     **/
/**                                                                        **/
/****************************************************************************/


/** @brief This struct singleton_t is used for creating a dummy model for the
 *  payload of singletonly created instance. You can modify the struct members
 *  as your needs.
  * @var param
  * Any parameter reference, which you may want to attech to so-called object.
  * @var value
  * Any value variable, which you
  *
*/
struct singleton_t
{
    void* param;
    int value;
};

/*! If we don't have any error type definition, autodefine it as int */
#ifndef error_t
#define error_t int
#endif


/*! Decide  between dynamic memory allocation and static memory allocation.
 * A platform layer implementation is also nicer as different needs
 * may occur like using memory tree or memory pool. */
#ifdef DYNAMIC_MEMORY_ALLOC
static singleton_ptr_t singleton_ptr = NULL;
#elif defined STATIC_MEMORY_ALLOC
static struct singleton_t singleton = {0};
#endif


/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/


/*!**************************************************************************
\brief Singleton_Create
-Function creates the singleton object instance if it does not exist.

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] - pointer to singleton object
@return -
*****************************************************************************/
singleton_ptr_t Singleton_Create()
{
#ifdef STATIC_MEMORY_ALLOC
    singleton_ptr_t singleton_ptr = &singleton;
#elif defined DYNAMIC_MEMORY_ALLOC
    if (singleton_ptr == NULL)
    {
        singleton_ptr = (singleton_ptr_t) SINGLETON_CALLOC(sizeof *singleton_ptr,
                        sizeof *singleton_ptr);
    }
#endif

    return singleton_ptr;
}


/*!**************************************************************************
\brief Singleton_GetInstance
-Function gets the singleton object instance

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - None
@param[in,out] -
@param[out] - pointer to singleton object
@return -
*****************************************************************************/
singleton_ptr_t Singleton_GetInstance()
{
#ifdef STATIC_MEMORY_ALLOC
    singleton_ptr_t singleton_ptr = &singleton;
#endif
    return singleton_ptr;
}


/*!**************************************************************************
\brief Singleton_Remove
-Function removes a singleton object instance.

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] - singleton_ptr_t: pointer to singleton object
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void Singleton_Remove(singleton_ptr_t singleton_ptr)
{
    if (singleton_ptr != NULL)
    {
#ifdef DYNAMIC_MEMORY_ALLOC
        SINGLETON_FREE(singleton_ptr);
#endif
        singleton_ptr = NULL;
    }
}


/*!**************************************************************************
\brief Singleton_SetPayload
-Function attaches a payload parameter to the singleton parameter instance

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] -a_singleton_ptr : pointer to singleton object,
            a_payload: payload argument
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void Singleton_SetPayload(singleton_ptr_t a_singleton_ptr, void* a_payload)
{
    a_singleton_ptr->param = a_payload;
}


/*!**************************************************************************
\brief Singleton_GetPayload
-Function gets the payload parameter to the singleton parameter instance

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] -a_singleton_ptr : pointer to singleton object,
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void* Singleton_GetPayload( const singleton_ptr_t a_singleton_ptr)
{
    return a_singleton_ptr->param;
}


/*!**************************************************************************
\brief Singleton_SetValue
-Function sets the value property of the singleton instance

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] -a_singleton_ptr : pointer to singleton object,
@param[in] -a_value:    value to attach
@param[in,out] -
@param[out] -
@return -
*****************************************************************************/
void Singleton_SetValue(singleton_ptr_t a_singleton_ptr, int a_value)
{
    a_singleton_ptr->value = a_value;
}


/*!**************************************************************************
\brief Singleton_GetValue
-Function gets the value property of the singleton instance

\author Ozen Ozkaya, 
\date 20.02.2017
@param[in] -a_singleton_ptr : pointer to singleton object,
@param[in,out] -
@param[out] -
@return -   value parameter of singleton object
*****************************************************************************/
int Singleton_GetValue(const singleton_ptr_t a_singleton_ptr)
{
    return a_singleton_ptr->value;
}



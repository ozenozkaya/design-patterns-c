/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "furniture_interface.h"
#include "template_factory.h"
#include "template_door.h"
#include "template_window.h"

#include "platform.h"

#define WOODEN


int main()
{
    /** This application is crationally an abstract factory implementation.*/
#ifdef WOODEN
    FACTORY_IMPORT(MODIFIER_INLINE,Wooden);
    factory_if_ptr_t factory_if_ptr = Factory_Create(Wooden);
#else
    FACTORY_IMPORT(MODIFIER_INLINE,Golden);
    factory_if_ptr_t factory_if_ptr = Factory_Create(Golden);
#endif


    furniture_ptr_t my_door = factory_if_ptr->door_if_ptr->create_func(80.0,180.0);
    furniture_ptr_t my_wind = factory_if_ptr->window_if_ptr->create_func(100.0,60.0);

    /*Here in show function you'll see the visitor behavior. Even input is door
     * or windows, we use same interface to show it.*/
    factory_if_ptr->door_if_ptr->show_func(my_door);
    factory_if_ptr->window_if_ptr->show_func(my_wind);

    /*Here in destroy function you'll see the visitor behavior. Even input is door
     * or windows, we use same interface to destroy it.*/
    factory_if_ptr->door_if_ptr->destroy_func(my_door);
    factory_if_ptr->window_if_ptr->destroy_func(my_wind);

    Factory_Destroy(factory_if_ptr);

    return 0;
}

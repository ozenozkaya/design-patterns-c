#ifdef __cplusplus
extern "C" {
#endif


#ifndef CUSTOMER_CATEGORIES_H
#define CUSTOMER_CATEGORIES_H

double PriceStrategy_Bronze(double amount, double shipping);

double PriceStrategy_Silver(double amount, double shipping);

double PriceStrategy_Gold(double amount, double shipping);

#endif

#ifdef __cplusplus
} // closing brace for extern "C"

#endif


#ifdef __cplusplus
extern "C" {
#endif

#ifndef ADDRESS_H
#define ADDRESS_H

typedef struct _address_t
{
  const char street_name;
} address_t, *address_ptr_t;

#endif

#ifdef __cplusplus
} // closing brace for extern "C"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include "address.h"
#include "customer_strategy.h"


typedef struct {
	unsigned long orderId;
	double amount;
	double shipping;
} order_t;

/* 
 A pointer to an incomplete type (hides the implementation details).
 */
typedef struct customer_t* customer_ptr_t;

customer_ptr_t Customer_Create(const char* name, const address_t* address,
		customer_price_strategy_func_t priceStrategy);

double Customer_PlaceOrder(customer_ptr_t customer, const order_t* order);

void Customer_ChangePriceCategory(customer_ptr_t customer,
		customer_price_strategy_func_t newPriceStrategy);

void Customer_Destroy(customer_ptr_t customer);

#endif


#ifdef __cplusplus
} // closing brace for extern "C"
#endif

#ifdef __cplusplus
extern "C" {
#endif


#ifndef CUSTOMER_STRATEGY_H
#define CUSTOMER_STRATEGY_H

typedef double (*customer_price_strategy_func_t)(double amount,
                                        double shipping);

#endif

#ifdef __cplusplus
} // closing brace for extern "C"
#endif

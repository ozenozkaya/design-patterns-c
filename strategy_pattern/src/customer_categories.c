#include "customer_categories.h"

/* In production code, I would definitely validate all input and
   secure the calculations upon entry of each function... */

double PriceStrategy_Bronze(double amount, double shipping)
{
  return amount * 0.98 + shipping;
}

double PriceStrategy_Silver(double amount, double shipping)
{
  return amount * 0.95 + shipping;
}

double PriceStrategy_Gold(double amount, double shipping)
{
  /* Free shipping for gold customers. */
  return amount * 0.90; 
}

#include "customer.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "platform.h"

typedef struct
{
    int implementation_of_linked_list;
} list_t;

struct customer_t
{
    const char* name;
    address_t address;
    list_t orders;

    /* Bind the strategy to Customer: */
    customer_price_strategy_func_t price_strategy_func;
};

customer_ptr_t Customer_Create(const char* name, const address_t* address, customer_price_strategy_func_t price_strategy_func)
{
    customer_ptr_t customer = PLATFORM_MALLOC(sizeof *customer);
    memset(customer,0,sizeof *customer);
    if (NULL != customer)
    {
        /* Bind the initial strategy supplied by the client. */
        customer->price_strategy_func = price_strategy_func;

        /* Initialize the other attributes of the customer here. */
    }

    return customer;
}

double Customer_PlaceOrder(customer_ptr_t customer, const order_t* order)
{
    double totalAmount = customer->price_strategy_func(order->amount, order->shipping);

    /* More code to process the order... */
    return totalAmount;
}

void Customer_ChangePriceCategory(customer_ptr_t customer, customer_price_strategy_func_t newPriceStrategy)
{
    assert(NULL != customer);
    customer->price_strategy_func = newPriceStrategy;
}

void Customer_Destroy(customer_ptr_t customer)
{
    if(customer)
    {
        memset(customer,0,sizeof *customer);
        PLATFORM_FREE(customer);
        customer=NULL;
    }
}

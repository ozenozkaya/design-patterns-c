#include "customer.h"
#include "customer_categories.h"
#include <stddef.h>
#include <stdio.h>
/*
 Client code illustrating the binding of a strategy:
 */
static customer_ptr_t createBronzeCustomer(const char* name, const address_t* address)
{
    return Customer_Create(name, address, PriceStrategy_Bronze);
}

static customer_ptr_t createSilverCustomer(const char* name, const address_t* address)
{
    return Customer_Create(name, address, PriceStrategy_Silver);
}

static customer_ptr_t createGoldCustomer(const char* name, const address_t* address)
{
    return Customer_Create(name, address, PriceStrategy_Gold);
}

int main()
{
    double productCost = 10.75;
    double shippingCost = 1.25;

    customer_ptr_t goldCustomer = createGoldCustomer("Mr Gold", NULL);
    customer_ptr_t silverCustomer = createSilverCustomer("Mr Silver", NULL);
    customer_ptr_t bronzeCustomer = createBronzeCustomer("Mr Bronze", NULL);

    order_t customerOrder =
        { .orderId = 0, .amount = productCost, .shipping = shippingCost };

    printf("Our product's cost is %f TL and shipping is %f TL.\r\n", productCost, shippingCost);
    printf("If you are a bronze customer you should totally pay %f TL\r\n", Customer_PlaceOrder(bronzeCustomer, &customerOrder));
    printf("If you are a silver customer you should totally pay %f TL\r\n", Customer_PlaceOrder(silverCustomer, &customerOrder));
    printf("If you are a gold customer you should totally pay %f\r TL\r\n", Customer_PlaceOrder(goldCustomer, &customerOrder));

    Customer_Destroy(bronzeCustomer);
    Customer_Destroy(silverCustomer);
    Customer_Destroy(goldCustomer);

    return 0;
}

#define _EDITOR_C
/** \file 
 * \brief This module contains the SAMPLE main entry point
 * \author 
 * \date 
*/
/*
 * editor.c
 *
 *  Created on: 28 Mar 2017
 *      Author: Özen Özkaya
 */

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "editor.h"
#include "memento.h"
#include "platform.h"



/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/

editor_ptr_t Editor_Create()
{
    editor_ptr_t editor_ptr = PLATFORM_MALLOC(sizeof *editor_ptr);
    memset(editor_ptr,0,sizeof(editor_t));
    memset(&editor_ptr->content,0,sizeof(content_t));
    return editor_ptr;
}

void Editor_Save(editor_ptr_t editor_ptr)
{
    Memento_Destroy(editor_ptr->memento_ptr);
    editor_ptr->memento_ptr = Memento_Create(&editor_ptr->content);
}

void Editor_Restore(editor_ptr_t editor_ptr)
{
    memcpy(editor_ptr->content.data,
                    editor_ptr->memento_ptr->content.data,
                    editor_ptr->memento_ptr->content.len);
    editor_ptr->content.len = editor_ptr->memento_ptr->content.len;
}

void Editor_TypeText(editor_ptr_t editor_ptr, const char* text)
{
    uint32_t new_len = editor_ptr->content.len + strlen(text) + 1;
    editor_ptr->content.data = PLATFORM_REALLOC((uint8_t*)editor_ptr->content.data,new_len);
    memset((uint8_t*)editor_ptr->content.data + editor_ptr->content.len, 0 , new_len-editor_ptr->content.len);
    strncat(((char*)editor_ptr->content.data),text,strlen(text));
    editor_ptr->content.len = new_len;
}

void Editor_ShowText(editor_ptr_t editor_ptr)
{
    printf("%s\r\n",(char*)editor_ptr->content.data);
}

void Editor_Destroy(editor_ptr_t editor_ptr )
{
    Memento_Destroy(editor_ptr->memento_ptr);
    if(editor_ptr)
    {
        PLATFORM_FREE(editor_ptr);
        editor_ptr = NULL;
    }
}
 
/****************************************************************************/
/**                                                                        **/
/**                LOCAL/PRIVATE FUNCTION IMPLEMENTATIONS                  **/
/**                                                                        **/
/****************************************************************************/

#define _MEMENTO_C
/** \file 
 * \brief This module contains the SAMPLE main entry point
 * \author 
 * \date 
*/
/*
 * memento.c
 *
 *  Created on: 28 Mar 2017
 *      Author: Özen Özkaya
 */

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "memento.h"
#include "platform.h"


/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
memento_ptr_t Memento_Create(content_ptr_t content_ptr)
{
    memento_ptr_t memento_ptr = PLATFORM_MALLOC(sizeof *memento_ptr);
    memento_ptr->content.data = PLATFORM_MALLOC(content_ptr->len);
    memcpy(memento_ptr->content.data,content_ptr->data,content_ptr->len);
    memento_ptr->content.len = content_ptr->len;
    return memento_ptr;
}

void Memento_Destroy(memento_ptr_t memento_ptr)
{
    if(memento_ptr)
    {
        if(memento_ptr->content.data)
        {
            PLATFORM_FREE(memento_ptr->content.data);
            memento_ptr->content.data=NULL;
        }
        PLATFORM_FREE(memento_ptr);
        memento_ptr = NULL;
    }
}
 
/****************************************************************************/
/**                                                                        **/
/**                LOCAL/PRIVATE FUNCTION IMPLEMENTATIONS                  **/
/**                                                                        **/
/****************************************************************************/

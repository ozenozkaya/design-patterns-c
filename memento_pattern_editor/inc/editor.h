#ifdef __cplusplus
extern "C" {
#endif

#ifndef EDITOR_H_
#define EDITOR_H_

#include "memento.h"

typedef struct _editor_t
{
    content_t content;
    memento_ptr_t memento_ptr;
}editor_t,*editor_ptr_t;

 
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/
editor_ptr_t Editor_Create();
void Editor_Save(editor_ptr_t editor_ptr);
void Editor_Restore(editor_ptr_t editor_ptr);
void Editor_TypeText(editor_ptr_t editor_ptr, const char* text);
void Editor_ShowText(editor_ptr_t editor_ptr);
void Editor_Destroy(editor_ptr_t editor_ptr );

#endif /* EDITOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

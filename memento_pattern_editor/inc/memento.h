#ifdef __cplusplus
extern "C" {
#endif

#ifndef MEMENTO_H_
#define MEMENTO_H_

#include <stdint.h>
/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

typedef struct _content_t
{
    void* data;
    uint32_t len;
}content_t, *content_ptr_t;

typedef struct _memento_t
{
    content_t content;
}memento_t,*memento_ptr_t;

 
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/
memento_ptr_t Memento_Create(content_ptr_t content_ptr);
void Memento_Destroy(memento_ptr_t memento_ptr);

#endif /* MEMENTO_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

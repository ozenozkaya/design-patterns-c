# MEMENTO DESIGN PATTERN 
Memento pattern is about capturing and storing the current state of an object in a manner that it can be restored later on in a smooth manner.

Wikipedia says

The memento pattern is a software design pattern that provides the ability to restore an object to its previous state (undo via rollback).

Lets take an example of text editor which keeps saving the state from time to time and that you can restore if you want.
/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include "editor.h"


int main()
{
    /**We are creating a so-called text editor*/
    editor_ptr_t editor_ptr = Editor_Create();

    /**A mysterious user types something*/
    Editor_TypeText(editor_ptr, "Hello ");
    /**We show what user wrote*/
    Editor_ShowText(editor_ptr);
    /**A mysterious user continues to type...*/
    Editor_TypeText(editor_ptr, "cruel world. ");
    /**We show what user wrote*/
    Editor_ShowText(editor_ptr);

    /**User saves the current editor status (with memento)*/
    Editor_Save(editor_ptr);
    /**A mysterious user continues to type...*/
    Editor_TypeText(editor_ptr, "Are you happy?");
    /**We show what user wrote*/
    Editor_ShowText(editor_ptr);

    /**User decides to restore saved state(with memento)*/
    Editor_Restore(editor_ptr);
    /**We show what user wrote*/
    Editor_ShowText(editor_ptr);

    /**We don't need the editor anymore*/
    Editor_Destroy(editor_ptr);
    return 0;
}

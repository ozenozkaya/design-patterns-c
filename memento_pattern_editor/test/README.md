## MEMENTO TEST APP

### How to build

gcc -std=c90 "-ID:\\workspace.despat\\memento_pattern_editor\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\editor.o" "..\\src\\editor.c" 

gcc -std=c90 "-ID:\\workspace.despat\\memento_pattern_editor\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "src\\memento.o" "..\\src\\memento.c" 

gcc -std=c90 "-ID:\\workspace.despat\\memento_pattern_editor\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Werror -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o memento_pattern_editor.exe "src\\editor.o" "src\\memento.o" "test\\main.o" 



### Function output

Hello 

Hello cruel world. 

Hello cruel world. Are you happy?

Hello cruel world. 



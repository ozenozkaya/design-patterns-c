
#ifdef __cplusplus
extern "C" {
#endif

#ifndef TEMPLATE_DOOR_FACTORY_H_
#define TEMPLATE_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include <factory_interface.h>
#include <stddef.h>
#include "platform.h"

#define FACTORY_IMPORT(FurnitureType)                                                                      \
    DOOR_IMPORT(FurnitureType)                                                                                   \
    WINDOW_IMPORT(FurnitureType)            \
    inline factory_if_ptr_t Factory_Create##FurnitureType()                                                \
    {                                                                                                       \
        factory_if_ptr_t factory_if_ptr = PLATFORM_MALLOC(sizeof *factory_if_ptr);   \
        factory_if_ptr->door_if_ptr = (furniture_if_ptr_t)PLATFORM_MALLOC(sizeof *factory_if_ptr->door_if_ptr);    \
        factory_if_ptr->door_if_ptr->create_func = FurnitureType ## Door_Create; \
        factory_if_ptr->door_if_ptr->destroy_func = FurnitureType ## Door_Destroy;   \
        factory_if_ptr->door_if_ptr->get_height_func = FurnitureType ## Door_GetHeightInCm;   \
        factory_if_ptr->door_if_ptr->show_func = FurnitureType ## Door_Show;                  \
        factory_if_ptr->door_if_ptr->get_width_func = FurnitureType ## Door_GetWidthInCm;     \
        factory_if_ptr->window_if_ptr = (furniture_if_ptr_t)PLATFORM_MALLOC(sizeof *factory_if_ptr->window_if_ptr);    \
        factory_if_ptr->window_if_ptr->create_func = FurnitureType ## Window_Create; \
        factory_if_ptr->window_if_ptr->destroy_func = FurnitureType ## Window_Destroy;   \
        factory_if_ptr->window_if_ptr->get_height_func = FurnitureType ## Window_GetHeightInCm;   \
        factory_if_ptr->window_if_ptr->show_func = FurnitureType ## Window_Show;                  \
        factory_if_ptr->window_if_ptr->get_width_func = FurnitureType ## Window_GetWidthInCm;     \
        return factory_if_ptr;                                                             \
    }

#define Factory_Destroy(factory_if_ptr)  \
{                                                                           \
    PLATFORM_FREE(factory_if_ptr->door_if_ptr);                        \
    PLATFORM_FREE(factory_if_ptr->window_if_ptr);                        \
    PLATFORM_FREE(factory_if_ptr);                                     \
    factory_if_ptr = NULL;                                             \
}

 #define Factory_Create(FurnitureType)     Factory_Create##FurnitureType()
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/


#endif /* TEMPLATE_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

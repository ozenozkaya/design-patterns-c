
#ifdef __cplusplus
extern "C" {
#endif


#ifndef INC_HOUSE_INTERFACE_H_
#define INC_HOUSE_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "furniture_interface.h"
#include <stdint.h>
/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

typedef void (*add_furniture_func_t)(void* house_if_ptr, furniture_ptr_t furniture_ptr);
typedef void (*list_furnitures_func_t)(void* house_if_ptr);
typedef  furniture_ptr_t* furniture_ptr_list_t;

typedef struct house_if_t
{
    furniture_ptr_list_t furniture_ptr_list;
    uint32_t  furniture_count;
    add_furniture_func_t add_furniture_func;
    list_furnitures_func_t list_furnitures_func;
    char* house_name;
}house_if_t, *house_if_ptr_t;


/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_FACTORY_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

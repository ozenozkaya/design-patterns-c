## COMPOSITE PATTERN TEST APP

### How to build

gcc "-ID:\\workspace.despat\\composite_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\house.o" "..\\src\\house.c" 

gcc "-ID:\\workspace.despat\\composite_pattern_door\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o composite_pattern_door.exe "src\\house.o" "test\\main.o" 




### Function output
There are totally 3 furniture in MyLovely House

*** Type:Door, Material:Wooden H:180.000000, W:80.000000

*** Type:Window, Material:Wooden H:60.000000, W:100.000000

*** Type:Door, Material:Wooden H:180.000000, W:80.000000
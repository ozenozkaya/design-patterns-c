/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "furniture_interface.h"
#include "template_factory.h"
#include "template_door.h"
#include "template_window.h"

#include "house.h"

int main()
{

    /** This application creationally contains an abstract factory pattern*/
    /*We create a Wooden furniture factory*/
    FACTORY_IMPORT(Wooden);
    factory_if_ptr_t factory_if_ptr = Factory_Create(Wooden);

    /**Then we create a wooden door with relevant dimensions*/
    furniture_ptr_t my_door = factory_if_ptr->door_if_ptr->create_func(80.0,180.0);
    /**Then we create a wooden window with relevant dimensions*/
    furniture_ptr_t my_wind = factory_if_ptr->window_if_ptr->create_func(100.0,60.0);


    {
        /**Then we create a House with its name "MyLovely House"*/
        house_if_ptr_t house_if_ptr = House_Create("MyLovely House");

        /**And we add furniture to the House. This is where we
         * have the composite behavior. When adding the furniture,
         * we can behave absolutely same to windows or doors. */
        house_if_ptr->add_furniture_func(house_if_ptr,my_door);
        house_if_ptr->add_furniture_func(house_if_ptr,my_wind);
        house_if_ptr->add_furniture_func(house_if_ptr,my_door);

        /*Then we list all the furniture in the house.*/
        house_if_ptr->list_furnitures_func(house_if_ptr);

        /*Even we did not wished to do so, we destroyed the House
         * to be able to prevent memory leakage.*/
        House_Destroy(house_if_ptr);
    }

    /** Then we destroy the furniture. Maybe it will be wiser to
     * destroy the doors and windows before destroying the house,
     * but who cares.*/
    factory_if_ptr->door_if_ptr->destroy_func(my_door);
    factory_if_ptr->window_if_ptr->destroy_func(my_wind);

    /** Lastly, we destroyed the factory as we dont need it
     * anymore. */
    Factory_Destroy(factory_if_ptr);

    /**You already know it*/
    return 42;
}

/*
 * house.c
 *
 *  Created on: 14 Mar 2017
 *      Author: Özen Özkaya
 */


#include "platform.h"
#include "house.h"
#include <string.h>
#include <stdio.h>

void House_AddFurniture(house_if_ptr_t a_house_if_ptr, furniture_ptr_t a_furniture_ptr);
void House_ListFurniture(house_if_ptr_t a_house_if_ptr);

house_if_ptr_t House_Create(const char* a_name)
{
    #define MAX_FURNITURE_NUM_PER_HOUSE 10

    house_if_ptr_t house_if_ptr = PLATFORM_MALLOC(sizeof *house_if_ptr);
    house_if_ptr->furniture_ptr_list = PLATFORM_MALLOC(MAX_FURNITURE_NUM_PER_HOUSE* (sizeof *(house_if_ptr->furniture_ptr_list)));
    memset(house_if_ptr->furniture_ptr_list, 0,MAX_FURNITURE_NUM_PER_HOUSE* (sizeof *(house_if_ptr->furniture_ptr_list)));
    house_if_ptr->add_furniture_func = (add_furniture_func_t)House_AddFurniture;
    house_if_ptr->list_furnitures_func = (list_furnitures_func_t)House_ListFurniture;
    house_if_ptr->house_name = (char*)a_name;
    house_if_ptr->furniture_count = 0;

    return house_if_ptr;
}

void House_AddFurniture(house_if_ptr_t a_house_if_ptr, furniture_ptr_t a_furniture_ptr)
{
    house_if_ptr_t house_if_ptr = (house_if_ptr_t)a_house_if_ptr;
    house_if_ptr->furniture_ptr_list[house_if_ptr->furniture_count] = a_furniture_ptr;
    house_if_ptr->furniture_count++;

}

void House_ListFurniture(house_if_ptr_t a_house_if_ptr)
{
    uint32_t i=0;
    printf("There are totally %d furniture in %s\r\n", a_house_if_ptr->furniture_count,
                    a_house_if_ptr->house_name);
    for(i=0;i<a_house_if_ptr->furniture_count; i++)
    {
        const char* materials[] = {"Wooden","Steel","Golden"};
        printf("*** Type:%s, Material:%s H:%f, W:%f\r\n",a_house_if_ptr->furniture_ptr_list[i]->furniture_type,
                        materials[a_house_if_ptr->furniture_ptr_list[i]->furniture_material_type],
                        a_house_if_ptr->furniture_ptr_list[i]->height_in_cm,
                        a_house_if_ptr->furniture_ptr_list[i]->width_in_cm);
    }
}

void House_Destroy(house_if_ptr_t house_if_ptr)
{
    PLATFORM_FREE(house_if_ptr->furniture_ptr_list);
    house_if_ptr->furniture_ptr_list = NULL;
    PLATFORM_FREE(house_if_ptr);
    house_if_ptr = NULL;
}

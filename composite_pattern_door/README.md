# COMPOSITE DESIGN PATTERN 
Composite pattern lets clients treat the individual objects in a uniform manner.

Wikipedia says

In software engineering, the composite pattern is a partitioning design pattern. The composite pattern describes that a group of objects is to be treated in the same way as a single instance of an object. The intent of a composite is to "compose" objects into tree structures to represent part-whole hierarchies. Implementing the composite pattern lets clients treat individual objects and compositions uniformly.

In this example, when adding furnitures to the house, we treated individual doors or windows in a uniform (or same) manner.

======================== Code Flow  =======================================================================

This application creationally contains an abstract factory pattern

Then we create a wooden door with relevant dimensions

Then we create a wooden window with relevant dimensions

Then we create a House with its name "MyLovely House"

And we add furniture to the House. This is where we have the composite behavior. When adding the furniture, we can behave absolutely same to windows or doors.

Then we destroy the furniture. Maybe it will be wiser to destroy the doors and windows before destroying the house, but who cares.

Lastly, we destroyed the factory as we dont need it anymore.

=============================================================================================================
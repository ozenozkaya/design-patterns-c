/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <furniture_interface.h>
#include "template_door.h"
#include "template_window.h"

#include "platform.h"


int main()
{
    DOOR_IMPORT(Golden);
    WINDOW_IMPORT(Golden);

    furniture_ptr_t golden_door_ptr = GoldenDoor_Create(120.0,220);
    /*This is (cloning) where essence of prototype pointer comes*/
    furniture_ptr_t clone_door_ptr = GoldenWindow_Clone(golden_door_ptr);
    clone_door_ptr->width_in_cm = 150.0;

    GoldenDoor_Show(golden_door_ptr);
    GoldenWindow_Show(clone_door_ptr);

    GoldenDoor_Destroy(golden_door_ptr);
    GoldenWindow_Destroy(clone_door_ptr);

    return 0;
}


#ifdef __cplusplus
extern "C" {
#endif

#ifndef GOLDEN_DOOR_H_
#define GOLDEN_DOOR_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include <factory_interface.h>

#define DOOR_IMPORT(DoorType)                       \
    inline furniture_ptr_t DoorType ## Door_Create(float width_in_cm, float height_in_cm)    \
    {                                                                                   \
        furniture_ptr_t door_ptr = PLATFORM_MALLOC(sizeof *door_ptr);                   \
        door_ptr->furniture_type = DoorType;                                            \
        door_ptr->width_in_cm = width_in_cm;                                            \
        door_ptr->height_in_cm = height_in_cm;                                          \
        /*Some more creation logic here*/                                               \
        return door_ptr;                                                                \
    }                                                                                   \
                                                                                        \
    inline float DoorType ## Door_GetWidthInCm(furniture_ptr_t door_ptr)                \
    {                                                                                   \
        return door_ptr->width_in_cm;                                                   \
    }                                                                                   \
                                                                                        \
    inline float DoorType ## Door_GetHeightInCm(furniture_ptr_t door_ptr)               \
    {                                                                                   \
        return door_ptr->height_in_cm;                                                  \
    }                                                                                   \
                                                                                        \
    inline void DoorType ## Door_Show(furniture_ptr_t door_ptr)                         \
    {                                                                                   \
        printf("Imagine a %s door with w=%f[cm] - h=%f[cm] \r\n",                       \
                        #DoorType,door_ptr->width_in_cm, door_ptr->height_in_cm);       \
    }                                                                                   \
                                                                                        \
    inline furniture_ptr_t DoorType ## Door_Clone(furniture_ptr_t prototype_door_ptr)   \
    {                                                                                   \
        furniture_ptr_t clone_door_ptr = PLATFORM_MALLOC(sizeof(furniture_t));          \
        memcpy(clone_door_ptr, prototype_door_ptr, sizeof(furniture_t));                \
        return  clone_door_ptr;                                                         \
    }                                                                                   \
    inline void DoorType ## Door_Destroy(furniture_ptr_t door_ptr)                      \
    {                                                                                   \
        PLATFORM_FREE(door_ptr);                                                        \
        door_ptr = NULL;                                                                \
    }

#endif /* GOLDEN_DOOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

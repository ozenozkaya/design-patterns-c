
#ifdef __cplusplus
extern "C" {
#endif

#ifndef TEMPLATE_WINDOW_H_
#define TEMPLATE_WINDOW_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include <factory_interface.h>

#define WINDOW_IMPORT(WindowType)                       \
    inline furniture_ptr_t WindowType ## Window_Create(float width_in_cm, float height_in_cm)    \
    {                                                                                   \
        furniture_ptr_t window_ptr = PLATFORM_MALLOC(sizeof *window_ptr);                        \
        window_ptr->furniture_type = WindowType;                                                   \
        window_ptr->width_in_cm = width_in_cm;                                            \
        window_ptr->height_in_cm = height_in_cm;                                          \
        /*Some more creation logic here*/                                               \
        return window_ptr;                                                                \
    }                                                                                   \
                                                                                        \
    inline float WindowType ## Window_GetWidthInCm(furniture_ptr_t window_ptr)                     \
    {                                                                                   \
        return window_ptr->width_in_cm;                                                   \
    }                                                                                   \
                                                                                        \
    inline float WindowType ## Window_GetHeightInCm(furniture_ptr_t window_ptr)                    \
    {                                                                                   \
        return window_ptr->height_in_cm;                                                  \
    }                                                                                   \
                                                                                        \
    inline void WindowType ## Window_Show(furniture_ptr_t window_ptr)                              \
    {                                                                                   \
        printf("Assume you have a %s window with w=%f[cm] - h=%f[cm] \r\n",                       \
                        #WindowType,window_ptr->width_in_cm, window_ptr->height_in_cm);       \
    }                                                                                   \
                                                                                        \
                                                                                        \
    inline furniture_ptr_t WindowType ## Window_Clone(furniture_ptr_t prototype_ptr)   \
    {                                                                                   \
        furniture_ptr_t clone_ptr = PLATFORM_MALLOC(sizeof *clone_ptr);       \
        memcpy(clone_ptr, prototype_ptr, sizeof(furniture_t));                  \
        return  clone_ptr;                                                         \
    }                                                                                   \
    inline void WindowType ## Window_Destroy(furniture_ptr_t window_ptr)                           \
    {                                                                                   \
        PLATFORM_FREE(window_ptr);                                                        \
        window_ptr = NULL;                                                                \
    }

#endif /* TEMPLATE_WINDOW_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

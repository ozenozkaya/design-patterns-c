#ifdef __cplusplus
extern "C" {
#endif


#ifndef FURNITURE_INTERFACE_H_
#define FURNITURE_INTERFACE_H_

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/
typedef enum
{
 Wooden,
 Steel,
 Golden
}furniture_material_types_t;

typedef struct
{
    float width_in_cm;
    float height_in_cm;
    furniture_material_types_t furniture_type;
}furniture_t,*furniture_ptr_t;

typedef furniture_ptr_t (*furniture_create_func_t)(float width_in_cm, float height_in_cm);
typedef float (*furniture_get_width_func_t)(furniture_ptr_t furniture_ptr);
typedef float (*furniture_get_height_func_t)(furniture_ptr_t furniture_ptr);
typedef void (*furniture_show_func_t)(furniture_ptr_t furniture_ptr);
typedef void (*furniture_destroy_func_t)(furniture_ptr_t furniture_ptr);

typedef struct
{
    furniture_create_func_t create_func;
    furniture_get_width_func_t   get_width_func;
    furniture_get_height_func_t get_height_func;
    furniture_show_func_t    show_func;
    furniture_destroy_func_t destroy_func;
}furniture_if_t,*furniture_if_ptr_t;

#endif /* FURNITURE_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

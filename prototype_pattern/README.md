# PROTOTYPE PATTERN (MACRO BASED IMPLEMENATATION)
The prototype pattern is a creational design pattern in software development. It is used when the type of objects to create is determined by a prototypical instance, which is cloned to produce new objects. ["Wikipage"]
The essence of the Prototype Pattern is to "Provide an interface for cloning objects without specifying their concrete classes."

In this implementation, prototype pattern  is implemented with macros.
Functionality and higher level model is similar with factrory method design pattern door example, but implementation is highly different. 

## BUILDER TEST APP

### How to build
gcc "-ID:\\workspace.despat\\factory_method_pattern_door4\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "test\\main.o" "..\\test\\main.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o factory_method_pattern_door4.exe "test\\main.o" 


### Function output
Imagine a Wooden door with w=80.000000[cm] - h=180.000000[cm] 

Imagine a Golden door with w=60.000000[cm] - h=160.000000[cm] 


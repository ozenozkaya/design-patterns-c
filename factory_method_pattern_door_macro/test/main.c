/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include <template_door_factory.h>

#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include <template_door.h>

#include "platform.h"

void Ozen_DoorShow(door_ptr_t door_ptr)
{
	printf("Kapina kurban: %f, %f\r\n",door_ptr->height_in_cm, door_ptr->width_in_cm);
}

int main()
{
    DOOR_FACTORY_IMPORT(Wooden);
    DOOR_FACTORY_IMPORT(Golden);

    door_factory_if_ptr_t wooden_door_factory_if_ptr = DoorFactory_Create(Wooden);
    door_factory_if_ptr_t golden_door_factory_if_ptr = DoorFactory_Create(Golden);

    door_ptr_t my_wooden_door = wooden_door_factory_if_ptr->door_if_ptr->door_create_func(80.0,180.0);
    door_ptr_t my_golden_door = golden_door_factory_if_ptr->door_if_ptr->door_create_func(60.0,160.0);

   wooden_door_factory_if_ptr->door_if_ptr->door_show_func=Ozen_DoorShow;

    wooden_door_factory_if_ptr->door_if_ptr->door_show_func(my_wooden_door);
    golden_door_factory_if_ptr->door_if_ptr->door_show_func(my_golden_door);

    wooden_door_factory_if_ptr->door_if_ptr->door_destroy_func(my_wooden_door);
    golden_door_factory_if_ptr->door_if_ptr->door_destroy_func(my_golden_door);

    DoorFactory_Destroy(wooden_door_factory_if_ptr);
    DoorFactory_Destroy(golden_door_factory_if_ptr);

    return 0;
}

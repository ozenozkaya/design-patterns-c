
#ifdef __cplusplus
extern "C" {
#endif

/*
 * door_interface.h
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef INC_DOOR_INTERFACE_H_
#define INC_DOOR_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include <door_interface.h>

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

typedef struct
{
    door_if_ptr_t door_if_ptr;
}door_factory_if_t, *door_factory_if_ptr_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_DOOR_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

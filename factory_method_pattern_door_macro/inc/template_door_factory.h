#ifdef __cplusplus
extern "C" {
#endif

#ifndef TEMPLATE_DOOR_FACTORY_H_
#define TEMPLATE_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"
#include <stddef.h>
#include "platform.h"

#define DOOR_FACTORY_IMPORT(DoorType)                                                                      \
    DOOR_IMPORT(DoorType)                                                                                   \
    inline door_factory_if_ptr_t DoorFactory_Create##DoorType()                                                \
    {                                                                                                       \
        door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);   \
        door_factory_if_ptr->door_if_ptr = (door_if_ptr_t)PLATFORM_MALLOC(sizeof *door_factory_if_ptr->door_if_ptr);    \
        door_factory_if_ptr->door_if_ptr->door_create_func = DoorType ## Door_Create; \
        door_factory_if_ptr->door_if_ptr->door_destroy_func = DoorType ## Door_Destroy;   \
        door_factory_if_ptr->door_if_ptr->door_get_height_func = DoorType ## Door_GetHeightInCm;   \
        door_factory_if_ptr->door_if_ptr->door_show_func = DoorType ## Door_Show;                  \
        door_factory_if_ptr->door_if_ptr->door_get_width_func = DoorType ## Door_GetWidthInCm;     \
        return door_factory_if_ptr;                                                             \
    }

#define DoorFactory_Destroy(door_factory_if_ptr)  \
{                                                                           \
    PLATFORM_FREE(door_factory_if_ptr->door_if_ptr);                        \
    PLATFORM_FREE(door_factory_if_ptr);                                     \
    door_factory_if_ptr = NULL;                                             \
}

 #define DoorFactory_Create(DoorType)     DoorFactory_Create##DoorType()
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/


#endif /* TEMPLATE_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

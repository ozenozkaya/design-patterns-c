/*
 * builder.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include "mcdonalds.h"

#include <stddef.h>
#include <stdlib.h>

static void McDonalds_PutBread(burger_ptr_t burger_ptr , char* bread);
static void McDonalds_PutSouce(burger_ptr_t burger_ptr , char* sauce);
static void McDonalds_PutMeat(burger_ptr_t burger_ptr , char* meat);
static burger_ptr_t McDonalds_BurgerCreate();

builder_if_ptr_t McDonalds_Create()
{
    builder_if_ptr_t builder_ptr = malloc(sizeof(builder_if_t));
    builder_ptr->put_bread_func = McDonalds_PutBread;
    builder_ptr->put_sauce_func = McDonalds_PutSouce;
    builder_ptr->put_meat_func = McDonalds_PutMeat;
    builder_ptr->burger_create_func = McDonalds_BurgerCreate;
    return builder_ptr;
}



void McDonalds_Destroy(builder_if_ptr_t builder_ptr)
{
    free(builder_ptr);
}

static burger_ptr_t McDonalds_BurgerCreate()
{
    return (burger_ptr_t)Burger_Create("McDonalds");
}


static void McDonalds_PutBread(burger_ptr_t burger_ptr , char* bread)
{
    burger_ptr->bread = bread;
}

static void McDonalds_PutSouce(burger_ptr_t burger_ptr , char* sauce)
{
    burger_ptr->sauce = sauce;
}

static void McDonalds_PutMeat(burger_ptr_t burger_ptr , char* meat)
{
    burger_ptr->meat = meat;
}



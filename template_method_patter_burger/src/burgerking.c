/*
 * builder.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include <stddef.h>
#include <stdlib.h>

#include "burgerking.h"

static void BurgerKing_PutBread(burger_ptr_t burger_ptr , char* part);
static void BurgerKing_PutSouce(burger_ptr_t burger_ptr , char* part);
static void BurgerKing_PutMeat(burger_ptr_t burger_ptr , char* meat);
static burger_ptr_t BurgerKing_BurgerCreate();


builder_if_ptr_t BurgerKing_Create()
{
    builder_if_ptr_t builder_ptr = malloc(sizeof(builder_if_t));
    builder_ptr->put_bread_func = BurgerKing_PutBread;
    builder_ptr->put_sauce_func = BurgerKing_PutSouce;
    builder_ptr->put_meat_func = BurgerKing_PutMeat;
    builder_ptr->burger_create_func = BurgerKing_BurgerCreate;
    return builder_ptr;
}

void BurgerKing_Destroy(builder_if_ptr_t builder_ptr)
{
    free(builder_ptr);
}

static burger_ptr_t BurgerKing_BurgerCreate()
{
    return (burger_ptr_t)Burger_Create("BurgerKing");
}


static void BurgerKing_PutBread(burger_ptr_t burger_ptr , char* bread)
{

    burger_ptr->bread = bread;
}

static void BurgerKing_PutSouce(burger_ptr_t burger_ptr , char* sauce)
{
    burger_ptr->sauce = sauce;
}

static void BurgerKing_PutMeat(burger_ptr_t burger_ptr , char* meat)
{
    burger_ptr->meat = meat;
}




/*
 * burger.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef BURGER_H_
#define BURGER_H_

typedef struct
{
    char* builder;
    char* bread;
    char* meat;
    char* sauce;
}burger_t, *burger_ptr_t;

burger_ptr_t Burger_Create(char* builder);
void Burger_Show(burger_ptr_t product_ptr);
void Burger_Destroy(burger_ptr_t product_ptr);


#endif /* BURGER_H_ */

/*
 * builder.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef MCDONALDS_H_
#define MCDONALDS_H_

#include "burger_interface.h"

builder_if_ptr_t McDonalds_Create();
void McDonalds_Destroy(builder_if_ptr_t builder_ptr);

#endif /* MCDONALDS_H_ */

#include "director.h"
#include "mcdonalds.h"
#include "burgerking.h"

#include <stdlib.h>
#include <stdint.h>
#include <time.h>
/*
 * main_app.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */


#define FAMILY_MEMBER_COUNT 4
const char* breads[]={"bugday_ekmegi","kepek_ekmegi","misir_ekmegi","cavdar_ekmegi","baget"};
const char* sauces[]={"mayonez","ketcap","bbq","hardal","ranch"};
const char* meats[]={"dana_eti","tavuk_eti","hindi_eti","balik_eti","kobe_eti"};

/**Creating a nice random burger is not an easy task.
 * This function FACADE'ly creates an easy interface for producing burger.*/
void Family_MakeRandomBurgers(burger_ptr_t* burger_ptr_list)
{
     uint32_t i=0;
     srand(time(NULL));
     for(i=0;i<FAMILY_MEMBER_COUNT;i++)
     {
         #define RANDOM_INDEX   rand()%FAMILY_MEMBER_COUNT
         /** This construction implementation itself internally shows
          * a template method pattern behavior.
          * see: builder_if_ptr_t and its components like:
          *     put_if_func_t put_bread_func;
          *     put_if_func_t put_sauce_func;
          *     put_if_func_t put_meat_func; */
         burger_ptr_list[i] = construct((char*)breads[RANDOM_INDEX],
                         (char*)sauces[RANDOM_INDEX],
                         (char*)meats[RANDOM_INDEX]);
         Burger_Show(burger_ptr_list[i]);
     }
 }

/** With this function, family members eat the burger,
 * at the end they just destroys the burger.*/
void Family_EatBurgers(burger_ptr_t* burger_ptr_list)
{
    uint32_t i=0;
    for(i=0;i<FAMILY_MEMBER_COUNT;i++)
    {
        Burger_Destroy(burger_ptr_list[i]);
    }
}

int main(void) {

    builder_if_ptr_t builderx_ptr = BurgerKing_Create();
    builder_if_ptr_t buildery_ptr = McDonalds_Create();
    builder_if_ptr_t builder_if_ptr_list[] = {builderx_ptr,buildery_ptr};
    burger_ptr_t   burger_ptr_list[FAMILY_MEMBER_COUNT];

    srand(time(NULL));
    /*We select a random burger producer.*/
    setBuilder(builder_if_ptr_list[rand()%2]);
    /*Then, producer FACADE'ly makes random burger.*/
    Family_MakeRandomBurgers(burger_ptr_list);
    /*Family ate delicious burger*/
    Family_EatBurgers(burger_ptr_list);

    /*We don't need producers anymore*/
    BurgerKing_Destroy(builderx_ptr);
    McDonalds_Destroy(buildery_ptr);

    return EXIT_SUCCESS;
}

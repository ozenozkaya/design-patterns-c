#define _SIMPLE_DOOR_FACTORY_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "door_factory_interface.h"
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
door_factory_if_ptr_t DoorFactory_CreateFactory()
{
    door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);
    door_factory_if_ptr->create_door_func = Door_Create;
    door_factory_if_ptr->get_height_func = Door_GetHeightInCm;
    door_factory_if_ptr->get_width_func = Door_GetWidthInCm;
    door_factory_if_ptr->show_door_func = (show_door_func_t)Door_Show;
    door_factory_if_ptr->destroy_door_func = Door_Destroy;
    return door_factory_if_ptr;
}

door_factory_if_ptr_t DoorFactory_CreateSecureFactory()
{
    door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);
    door_factory_if_ptr->create_door_func = Door_Create;
    door_factory_if_ptr->get_height_func = Door_GetHeightInCm;
    door_factory_if_ptr->get_width_func = Door_GetWidthInCm;
    door_factory_if_ptr->show_door_func = (show_door_func_t)SecureDoor_Show;
    door_factory_if_ptr->destroy_door_func = Door_Destroy;
    return door_factory_if_ptr;
}

void DoorFactory_DestroyFactory(door_factory_if_ptr_t door_factory_if_ptr)
{
    PLATFORM_FREE(door_factory_if_ptr);
    door_factory_if_ptr = NULL;
}
 

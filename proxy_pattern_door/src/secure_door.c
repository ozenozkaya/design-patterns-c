#define _DOOR_C
/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "door.h"
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/

/** Here you'll find the proxy behavior. We tunelled the door show function
 * with so-called simple security functionality.*/
void SecureDoor_Show(door_ptr_t door_ptr, const char* pass)
{
    if(pass && !strcmp(pass,"$ecr@t"))
    {
        Door_Show(door_ptr,(void*)pass);
    }
    else
    {
        printf("No access!\r\n");
    }
}




/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include "simple_door_factory.h"
#include <stddef.h>

int main()
{
    door_factory_if_ptr_t door_factory_if_ptr = DoorFactory_CreateSecureFactory();

    door_ptr_t my_wooden_door = door_factory_if_ptr->create_door_func(WOODEN_DOOR,80.0,180.0);
    door_ptr_t my_golden_door = door_factory_if_ptr->create_door_func(GOLDEN_DOOR,60.0,160.0);

    door_factory_if_ptr->show_door_func(my_wooden_door, NULL);
    door_factory_if_ptr->show_door_func(my_golden_door, "$ecr@t");

    door_factory_if_ptr->destroy_door_func(my_wooden_door);
    door_factory_if_ptr->destroy_door_func(my_golden_door);

    DoorFactory_DestroyFactory(door_factory_if_ptr);

    return 0;
}

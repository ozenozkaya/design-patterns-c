#ifdef __cplusplus
extern "C" {
#endif


#ifndef DOOR_H_
#define DOOR_H_

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/
typedef enum
{
 WOODEN_DOOR,
 STEEL_DOOR,
 GOLDEN_DOOR
}door_types_t;

typedef struct
{
    float width_in_cm;
    float height_in_cm;
    door_types_t door_type;
}door_t,*door_ptr_t;
 
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/
door_ptr_t Door_Create(door_types_t door_type, float width_in_cm, float height_in_cm);
float Door_GetWidthInCm(door_ptr_t door_ptr);
float Door_GetHeightInCm(door_ptr_t door_ptr);
void Door_Show(door_ptr_t door_ptr,  void* param);
void SecureDoor_Show(door_ptr_t door_ptr, const char* pass);
void Door_Destroy(door_ptr_t door_ptr);

#endif /* DOOR_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

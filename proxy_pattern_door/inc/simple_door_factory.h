#ifdef __cplusplus
extern "C" {
#endif

#ifndef SIMPLE_DOOR_FACTORY_H_
#define SIMPLE_DOOR_FACTORY_H_

/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door_factory_interface.h"

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

door_factory_if_ptr_t DoorFactory_CreateFactory();
door_factory_if_ptr_t DoorFactory_CreateSecureFactory();
void DoorFactory_DestroyFactory(door_factory_if_ptr_t door_factory_if_ptr);

#endif /* SIMPLE_DOOR_FACTORY_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

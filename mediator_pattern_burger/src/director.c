/*
 ============================================================================
 Name        : builder_pat.c
 Author      : �zen �zkaya
 Version     :
 Copyright   : No rights reserved
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "burgerking.h"
#include "mcdonalds.h"


/** First parameter of this function is builder_if_ptr and it works like a mediator.*/
burger_ptr_t construct(builder_if_ptr_t builder_if_ptr, char* bread, char* sauce, char* meat)
{
    burger_ptr_t product_ptr;
    product_ptr = builder_if_ptr->burger_create_func();
    builder_if_ptr->put_bread_func(product_ptr, bread);
    builder_if_ptr->put_sauce_func(product_ptr, sauce);
    builder_if_ptr->put_meat_func(product_ptr, meat);
    return product_ptr;
}

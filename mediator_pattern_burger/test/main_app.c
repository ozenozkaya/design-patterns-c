#include "director.h"
#include "mcdonalds.h"
#include "burgerking.h"

#include <stdlib.h>
/*
 * main_app.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

int main(void) {

    /** This application creationally implements builder pattern.*/
    builder_if_ptr_t builderx_ptr = BurgerKing_Create();
    builder_if_ptr_t buildery_ptr = McDonalds_Create();
    burger_ptr_t   product_ptr_A,product_ptr_B,product_ptr_C,product_ptr_D;

    /** Construct function shows a mediator behavior and first parameter of
     * the construct function actually takes the builder as mediator. */
    product_ptr_A = construct(builderx_ptr, "bugday_ekmegi","ketcap","et");
    product_ptr_B = construct(builderx_ptr, "kepek_ekmegi","mayonez","tavuk");

    product_ptr_C = construct(buildery_ptr, "misir_ekmegi","bbq","et");
    product_ptr_D = construct(buildery_ptr, "cavdar_ekmegi","hardal","hindi");

    Burger_Show(product_ptr_A);
    Burger_Show(product_ptr_B);
    Burger_Show(product_ptr_C);
    Burger_Show(product_ptr_D);

    Burger_Destroy(product_ptr_A);
    Burger_Destroy(product_ptr_B);
    Burger_Destroy(product_ptr_C);
    Burger_Destroy(product_ptr_D);

    BurgerKing_Destroy(builderx_ptr);
    McDonalds_Destroy(buildery_ptr);

    return EXIT_SUCCESS;
}

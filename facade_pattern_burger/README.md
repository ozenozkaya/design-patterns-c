# FACADE DESIGN PATTERN 
Facade pattern provides a simplified interface to a complex subsystem.

Wikipedia says

A facade is an object that provides a simplified interface to a larger body of code, such as a class library.

In this example, we are creating a facade to easily present burger preperation for a family.
(Application creationally implements a builder pattern.)

## COMPOSITE PATTERN TEST APP

### How to build

gcc -std=c90 "-ID:\\workspace.despat\\facade_pat_burger\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\burgerking.o" "..\\src\\burgerking.c" 

gcc -std=c90 "-ID:\\workspace.despat\\facade_pat_burger\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\mcdonalds.o" "..\\src\\mcdonalds.c" 

gcc -std=c90 "-ID:\\workspace.despat\\facade_pat_burger\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\director.o" "..\\src\\director.c" 

gcc -std=c90 "-ID:\\workspace.despat\\facade_pat_burger\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "test\\main_app.o" "..\\test\\main_app.c" 

gcc -std=c90 "-ID:\\workspace.despat\\facade_pat_burger\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "src\\burger.o" "..\\src\\burger.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o facade_pat_burger.exe "src\\burger.o" "src\\burgerking.o" "src\\director.o" "src\\mcdonalds.o" "test\\main_app.o" 



### Function output (formatting problems may occur, download this readme to see the proper format)

Adding bread... Adding sauce... Adding meat... Some final magic...

~BurgerKing kkd-Burger~
/|||||||||||||||||||\	 kepek_ekmegi
ooooooooooooooooooooo	 ketcap
:::::::::::::::::::::	 dana_eti 
\|||||||||||||||||||/	 kepek_ekmegi

Adding bread... Adding sauce... Adding meat... Some final magic...

~BurgerKing cmd-Burger~
/|||||||||||||||||||\	 cavdar_ekmegi
ooooooooooooooooooooo	 mayonez
:::::::::::::::::::::	 dana_eti 
\|||||||||||||||||||/	 cavdar_ekmegi

Adding bread... Adding sauce... Adding meat... Some final magic...

~BurgerKing chb-Burger~
/|||||||||||||||||||\	 cavdar_ekmegi
ooooooooooooooooooooo	 hardal
:::::::::::::::::::::	 balik_eti 
\|||||||||||||||||||/	 cavdar_ekmegi

Adding bread... Adding sauce... Adding meat... Some final magic...

~BurgerKing bbh-Burger~
/|||||||||||||||||||\	 bugday_ekmegi
ooooooooooooooooooooo	 bbq
:::::::::::::::::::::	 hindi_eti 
\|||||||||||||||||||/	 bugday_ekmegi

/*
 ============================================================================
 Name        : builder_pat.c
 Author      : Özen Özkaya
 Version     :
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "burgerking.h"
#include "mcdonalds.h"

static builder_if_ptr_t builder_if_ptr;


void setBuilder(builder_if_ptr_t new_builder_if_ptr)
{
    builder_if_ptr = new_builder_if_ptr;
}

burger_ptr_t construct(char* bread, char* sauce, char* meat)
{
    burger_ptr_t product_ptr;
    product_ptr = builder_if_ptr->burger_create_func();
    printf("Adding bread... ");
    builder_if_ptr->put_bread_func(product_ptr, bread);
    printf("Adding sauce... ");
    builder_if_ptr->put_sauce_func(product_ptr, sauce);
    printf("Adding meat... ");
    builder_if_ptr->put_meat_func(product_ptr, meat);
    printf("Some final magic...\r\n");
    return product_ptr;
}

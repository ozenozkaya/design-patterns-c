/*
 * burger.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include "burger.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

burger_ptr_t Burger_Create(char* builder)
{
    burger_ptr_t product_ptr = malloc(sizeof(burger_t));
    product_ptr->builder = builder;
    return product_ptr;

}

void Burger_Destroy(burger_ptr_t burger_ptr)
{
    if(burger_ptr != NULL)
    {
        free(burger_ptr);
        burger_ptr=NULL;
    }
}


void Burger_Show(burger_ptr_t product_ptr)
{
    printf("~%s %c%c%c-Burger~\r",product_ptr->builder,
                    product_ptr->bread[0],
                    product_ptr->sauce[0],
                    product_ptr->meat[0]);
    printf("/|||||||||||||||||||\\\t %s\r",product_ptr->bread);
    printf("ooooooooooooooooooooo\t %s\r",product_ptr->sauce);
    printf(":::::::::::::::::::::\t %s \r",product_ptr->meat);
    printf("\\|||||||||||||||||||/\t %s\r\n",product_ptr->bread);
}


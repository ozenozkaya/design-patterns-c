/*
 * builder.h
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef BURGERKING_H_
#define BURGERKING_H_

#include "burger_interface.h"

builder_if_ptr_t BurgerKing_Create();
void BurgerKing_Destroy(builder_if_ptr_t builder_ptr);

#endif /* BUILDERX_H_ */

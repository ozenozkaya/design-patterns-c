#ifdef __cplusplus
extern "C" {
#endif

#ifndef BURGER_PLATFORM_H_
#define BURGER_PLATFORM_H_

/*! MALLOC mapping*/
#define BURGER_MALLOC    malloc

/*! CALLOC mapping*/
#define BURGER_CALLOC    calloc

/*! FREE mapping*/
#define BURGER_FREE      free

#endif /* BURGER_PLATFORM_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"

#endif

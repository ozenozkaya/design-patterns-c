/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include "simple_door_factory.h"
#include "platform.h"
#include <stdio.h>

void Ali_WeHaveANewDoor(void* payload)
{
    door_ptr_t door = (door_ptr_t)payload;
    printf("Hey Ali, we have a new door, :");

    Door_Show(door);
}

void Veli_WeHaveANewDoor(void* payload)
{
    door_ptr_t door = (door_ptr_t)payload;
    printf("Hey Veli, we have a new door, :");
    Door_Show(door);
}

int main()
{
    door_factory_if_ptr_t door_factory_if_ptr = DoorFactory_CreateFactory();

    customer_ptr_t ali = PLATFORM_MALLOC(sizeof(customer_t));
    customer_ptr_t veli = PLATFORM_MALLOC(sizeof(customer_t));



    ali->name = "Ali";
    ali->notify_func =Ali_WeHaveANewDoor;

    veli->name = "Veli";
    veli->notify_func = Veli_WeHaveANewDoor;

    DoorFactory_RegisterCustomer(door_factory_if_ptr,ali);
    DoorFactory_RegisterCustomer(door_factory_if_ptr,veli);

    {
        door_ptr_t my_wooden_door = door_factory_if_ptr->create_door_func(door_factory_if_ptr,WOODEN_DOOR,80.0,180.0);
        door_ptr_t my_golden_door = door_factory_if_ptr->create_door_func(door_factory_if_ptr,GOLDEN_DOOR,60.0,160.0);


        door_factory_if_ptr->destroy_door_func(my_wooden_door);
        door_factory_if_ptr->destroy_door_func(my_golden_door);
    }

    PLATFORM_FREE((void*)ali);
    PLATFORM_FREE((void*)veli);

    DoorFactory_DestroyFactory(door_factory_if_ptr);

    return 0;
}

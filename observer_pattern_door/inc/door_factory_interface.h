#ifdef __cplusplus
extern "C" {
#endif

/*
 * door_interface.h
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#ifndef INC_DOOR_INTERFACE_H_
#define INC_DOOR_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/
#include "door.h"
#include "customer.h"
#include <stdint.h>
#include <stdbool.h>

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/

typedef struct _door_factory_if_t* door_factory_if_ptr_t;

typedef float (*door_get_dimension_func_t)();
typedef door_ptr_t (*create_door_func_t)(door_factory_if_ptr_t door_factory_if_ptr,
                door_types_t door_type,
                float width_in_cm, float height_in_cm);
typedef void (*show_door_func_t)(door_ptr_t door_ptr);
typedef void (*destroy_door_func_t)(door_ptr_t door_ptr);
typedef bool (*register_customer_func_t)(door_factory_if_ptr_t door_factory_if_ptr, customer_ptr_t customer_ptr);

typedef struct _door_factory_if_t
{
    door_get_dimension_func_t get_width_func;
    door_get_dimension_func_t get_height_func;
    create_door_func_t create_door_func;
    show_door_func_t show_door_func;
    destroy_door_func_t destroy_door_func;
    register_customer_func_t register_customer_func;
    customer_ptr_t *customer_ptr_list;
    uint32_t customer_cnt;
}door_factory_if_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_DOOR_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * customer.h
 *
 *  Created on: 28 Mar 2017
 *      Author: Özen Özkaya
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/
typedef void (*customer_notify_func_t)(void* payload);

typedef struct _customer_t
{
    char* name;
    customer_notify_func_t notify_func;
}customer_t, *customer_ptr_t;
  
 
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* CUSTOMER_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

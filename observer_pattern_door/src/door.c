#define _DOOR_C
/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "door.h"
#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
  
door_ptr_t Door_Create(door_types_t door_type, float width_in_cm, float height_in_cm)
{
    door_ptr_t door_ptr = PLATFORM_MALLOC(sizeof *door_ptr);
    door_ptr->door_type = door_type;
    door_ptr->width_in_cm = width_in_cm;
    door_ptr->height_in_cm = height_in_cm;
    /*Some more creation logic here*/
    return door_ptr;
}

float Door_GetWidthInCm(door_ptr_t door_ptr)
{
    return door_ptr->width_in_cm;
}

float Door_GetHeightInCm(door_ptr_t door_ptr)
{
    return door_ptr->height_in_cm;
}

void Door_Show(door_ptr_t door_ptr)
{
    switch(door_ptr->door_type)
    {
    case WOODEN_DOOR:
        printf("Imagine a Wooden door with w=%f[cm] - h=%f[cm] \r\n",
                        door_ptr->width_in_cm, door_ptr->height_in_cm);
        break;
    case STEEL_DOOR:
        printf("Imagine a Steel door with w=%f[cm] - h=%f[cm] \r\n",
                        door_ptr->width_in_cm, door_ptr->height_in_cm);
        break;
    case GOLDEN_DOOR:
        printf("Imagine a Golden door with w=%f[cm] - h=%f[cm] \r\n",
                        door_ptr->width_in_cm, door_ptr->height_in_cm);
        break;
    }
}

void Door_Destroy(door_ptr_t door_ptr)
{
    PLATFORM_FREE(door_ptr);
    door_ptr = NULL;
}
 


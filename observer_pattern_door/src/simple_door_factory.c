#define _SIMPLE_DOOR_FACTORY_C

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "simple_door_factory.h"
#include "platform.h"

bool DoorFactory_RegisterCustomer(door_factory_if_ptr_t door_factory_if_ptr, customer_ptr_t customer_ptr);
door_ptr_t DoorFactory_DoorCreate(door_factory_if_ptr_t door_factory_if_ptr,
                door_types_t door_type,
                float width_in_cm,
                float height_in_cm);

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
door_factory_if_ptr_t DoorFactory_CreateFactory()
{
    door_factory_if_ptr_t door_factory_if_ptr = PLATFORM_MALLOC(sizeof *door_factory_if_ptr);
    door_factory_if_ptr->create_door_func = DoorFactory_DoorCreate;
    door_factory_if_ptr->get_height_func = Door_GetHeightInCm;
    door_factory_if_ptr->get_width_func = Door_GetWidthInCm;
    door_factory_if_ptr->show_door_func = Door_Show;
    door_factory_if_ptr->destroy_door_func = Door_Destroy;
    door_factory_if_ptr->register_customer_func = DoorFactory_RegisterCustomer;
    door_factory_if_ptr->customer_ptr_list = PLATFORM_MALLOC(sizeof(customer_ptr_t) * MAX_REGISTERED_OBSERVER);
    memset(door_factory_if_ptr->customer_ptr_list,0,sizeof(customer_ptr_t) * MAX_REGISTERED_OBSERVER);
    door_factory_if_ptr->customer_cnt=0;
    return door_factory_if_ptr;
}

bool DoorFactory_RegisterCustomer(door_factory_if_ptr_t door_factory_if_ptr, customer_ptr_t customer_ptr)
{
    if(door_factory_if_ptr->customer_cnt < MAX_REGISTERED_OBSERVER)
    {
        door_factory_if_ptr->customer_ptr_list[door_factory_if_ptr->customer_cnt] = customer_ptr;
        door_factory_if_ptr->customer_cnt++;
        return true;
    }
    return false;
}

door_ptr_t DoorFactory_DoorCreate(door_factory_if_ptr_t door_factory_if_ptr,
                door_types_t door_type,
                float width_in_cm,
                float height_in_cm)
{
    door_ptr_t door_ptr = Door_Create(door_type,width_in_cm,height_in_cm);

    {
        uint32_t i=0;
        for(i=0; i< door_factory_if_ptr->customer_cnt; i++)
        {
            door_factory_if_ptr->customer_ptr_list[i]->notify_func((void*)door_ptr);
        }
    }

    return door_ptr;
}
 


void DoorFactory_DestroyFactory(door_factory_if_ptr_t door_factory_if_ptr)
{
    if(door_factory_if_ptr->customer_ptr_list)
    {
        PLATFORM_FREE(door_factory_if_ptr->customer_ptr_list);
        door_factory_if_ptr->customer_ptr_list=NULL;
    }

    if(door_factory_if_ptr)
    {
        PLATFORM_FREE(door_factory_if_ptr);
        door_factory_if_ptr=NULL;
    }
}


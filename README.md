## Synopsis

Design Pattern library which is written with ANSI C.

If you have no ideas about what is design patterns, start reading [this](https://github.com/kamranahmedse/design-patterns-for-humans). 

## Code Example

Each folder includes an eclipse project and all required source files. Under test folders you can also find example codes, how to build them and expected output.

## Motivation
People tend to think that with ANSI C, it is not possible to implement full set of design patterns. One of the motivations of this study to prove that IT IS FULLY POSSIBLE TO IMPLEMENT GoF DESIGN PATTERNS WITH ANSI C.

The MAIN motivation is to use the library in embedded systems with various purposes on the way of systemmatic design.
Educational content will be also encouraged.

## Installation

You can directly import projects to eclipse cdt.

## Contributors

Time will give us the answer. Till now:

- Özen Özkaya


## License

[Beerware License](https://en.wikipedia.org/wiki/Beerware)



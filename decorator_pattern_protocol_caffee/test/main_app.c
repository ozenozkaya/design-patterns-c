#include <coffee.h>
#include <stdlib.h>
#include "coffee.h"


int main(void)
{
    {
        coffee_ptr_t latte_coffee_ptr = Coffee_DecorateMilkCoffee(Coffee_CreateSimpleCoffee());
        Coffee_Show(latte_coffee_ptr);
        Coffee_Destroy(latte_coffee_ptr);
    }
    {
        coffee_ptr_t doublelatte_coffee_ptr = Coffee_DecorateMilkCoffee(Coffee_DecorateMilkCoffee(Coffee_CreateSimpleCoffee()));
        Coffee_Show(doublelatte_coffee_ptr);
        Coffee_Destroy(doublelatte_coffee_ptr);
    }
    {
        coffee_ptr_t milky_creamy_coffee_ptr = Coffee_DecorateCremeCoffee(Coffee_DecorateMilkCoffee(Coffee_CreateSimpleCoffee()));
        Coffee_Show(milky_creamy_coffee_ptr);
        Coffee_Destroy(milky_creamy_coffee_ptr);
    }
    {
        coffee_ptr_t milky_sweet_coffee_ptr = Coffee_DecorateSugarCoffee(Coffee_DecorateMilkCoffee(Coffee_CreateSimpleCoffee()));
        Coffee_Show(milky_sweet_coffee_ptr);
        Coffee_Destroy(milky_sweet_coffee_ptr);
    }
    {
        coffee_ptr_t combo_caffee_ptr = Coffee_DecorateCremeCoffee(Coffee_DecorateSugarCoffee(Coffee_DecorateMilkCoffee(Coffee_CreateSimpleCoffee())));
        Coffee_Show(combo_caffee_ptr);
        Coffee_Destroy(combo_caffee_ptr);
    }
    return EXIT_SUCCESS;
}



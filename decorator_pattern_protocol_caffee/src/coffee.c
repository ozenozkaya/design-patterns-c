/*
 * product.c
 *
 *  Created on: 17 ?ub 2017
 *      Author: Özen Özkaya
 */
#include <coffee.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "platform.h"

#define SIMPLE_COFFEE_DESC "A simple coffee"
#define MILK_COFFEE_DESC " + milk"
#define SUGAR_COFFEE_DESC " + sugar"
#define CREME_COFFEE_DESC " + creme"


coffee_ptr_t Coffee_CreateSimpleCoffee()
{
    coffee_ptr_t caffee_ptr = PLATFORM_MALLOC(sizeof(coffee_t));
    caffee_ptr->description = (char*)PLATFORM_MALLOC(strlen(SIMPLE_COFFEE_DESC)+1);
    memset(caffee_ptr->description,0,strlen(SIMPLE_COFFEE_DESC)+1);
    strcpy(caffee_ptr->description,SIMPLE_COFFEE_DESC);
    caffee_ptr->cost_in_tl = COFFE_COST_IN_TL;
    return caffee_ptr;

}

void Coffee_Destroy(coffee_ptr_t coffee_ptr)
{
    if(coffee_ptr != NULL)
    {
        if(coffee_ptr->description != NULL)
        {
            PLATFORM_FREE(coffee_ptr->description);
            coffee_ptr->description = NULL;
        }
        PLATFORM_FREE(coffee_ptr);
        coffee_ptr=NULL;
    }
}

coffee_ptr_t Coffee_DecorateMilkCoffee(coffee_ptr_t coffee_ptr)
{
    coffee_ptr_t milk_coffee_ptr = PLATFORM_MALLOC(sizeof *milk_coffee_ptr);
    uint32_t description_len = strlen(coffee_ptr->description)+1;

    description_len += strlen(MILK_COFFEE_DESC)+1;

    milk_coffee_ptr->description = (char*)PLATFORM_MALLOC(description_len);

    strcpy(milk_coffee_ptr->description,coffee_ptr->description);
    strncat(milk_coffee_ptr->description,MILK_COFFEE_DESC,strlen(MILK_COFFEE_DESC)+1);

    milk_coffee_ptr->cost_in_tl =coffee_ptr->cost_in_tl + MILK_COST_IN_TL;

    Coffee_Destroy(coffee_ptr);
    return milk_coffee_ptr;
}

coffee_ptr_t Coffee_DecorateSugarCoffee(coffee_ptr_t coffee_ptr)
{
    coffee_ptr_t sugar_coffee_ptr = PLATFORM_MALLOC(sizeof *sugar_coffee_ptr);
    uint32_t description_len = strlen(coffee_ptr->description)+1;

    description_len += strlen(SUGAR_COFFEE_DESC)+1;

    sugar_coffee_ptr->description = (char*)PLATFORM_MALLOC(description_len);

    strcpy(sugar_coffee_ptr->description,coffee_ptr->description);
    strncat(sugar_coffee_ptr->description,SUGAR_COFFEE_DESC,strlen(SUGAR_COFFEE_DESC)+1);

    sugar_coffee_ptr->cost_in_tl =coffee_ptr->cost_in_tl + SUGAR_COST_IN_TL;

    Coffee_Destroy(coffee_ptr);
    return sugar_coffee_ptr;
}

coffee_ptr_t Coffee_DecorateCremeCoffee(coffee_ptr_t coffee_ptr)
{
    coffee_ptr_t sugar_coffee_ptr = PLATFORM_MALLOC(sizeof *sugar_coffee_ptr);
    uint32_t description_len = strlen(coffee_ptr->description)+1;

    description_len += strlen(CREME_COFFEE_DESC)+1;

    sugar_coffee_ptr->description = (char*)PLATFORM_MALLOC(description_len);

    strcpy(sugar_coffee_ptr->description,coffee_ptr->description);
    strncat(sugar_coffee_ptr->description,CREME_COFFEE_DESC,strlen(CREME_COFFEE_DESC)+1);

    sugar_coffee_ptr->cost_in_tl =coffee_ptr->cost_in_tl + CREME_COST_IN_TL;
    Coffee_Destroy(coffee_ptr);
    return sugar_coffee_ptr;
}


void Coffee_ShowDecription(coffee_ptr_t coffee_ptr)
{
    printf("Desc: %s\r\n",coffee_ptr->description);
}

void Coffee_ShowCost(coffee_ptr_t coffee_ptr)
{
    printf("Cost: %d[TL]\r\n",coffee_ptr->cost_in_tl);
}

void Coffee_Show(coffee_ptr_t coffee_ptr)
{
    Coffee_ShowDecription(coffee_ptr);
    Coffee_ShowCost(coffee_ptr);
}

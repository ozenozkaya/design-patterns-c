#define _INVOKER_C
/** \file 
 * \brief This module contains the SAMPLE main entry point
 * \author Özen Özkaya
 * \date 
*/
/*
 * invoker.c
 *
 *  Created on: 20 Mar 2017
 *      Author: Özen Özkaya
 */

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "command.h"


/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/
void RemoteControl_SubmitCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr)
{
    printf("Submitting command %s ... ",command_if_ptr->cmd_name);
    command_if_ptr->execute_func(target_obj_ptr);
}

void RemoteControl_UndoCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr)
{
    printf("Undo command %s ... ",command_if_ptr->cmd_name);
    command_if_ptr->undo_func(target_obj_ptr);
}

void RemoteControl_RedoCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr)
{
    printf("Redo command %s ... ",command_if_ptr->cmd_name);
    command_if_ptr->redo_func(target_obj_ptr);
}
 

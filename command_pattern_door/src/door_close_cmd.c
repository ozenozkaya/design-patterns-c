#define _DOOR_CLOSE_C
/** \file 
 * \brief This module contains the SAMPLE main entry point
 * \author  Özen Özkaya
 * \date 
*/
/*
 * open.c
 *
 *  Created on: 20 Mar 2017
 *      Author: Özen Özkaya
 */

/****************************************************************************/
/**                                                                        **/
/**                           MODULES USED                                 **/
/**                                                                        **/
/****************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>

#include "command.h"
#include "door.h"

#include "platform.h"

/****************************************************************************/
/**                                                                        **/
/**                  EXPORTED/PUBLIC FUNCTION IMPLEMENTATIONS              **/
/**                                                                        **/
/****************************************************************************/

command_if_ptr_t DoorCloseCmd_Create()
{
    command_if_ptr_t command_if_ptr = PLATFORM_MALLOC(sizeof *command_if_ptr);
    command_if_ptr->execute_func = (execute_func_t) Door_Close;
    command_if_ptr->redo_func = (redo_func_t) Door_Close;
    command_if_ptr->undo_func = (undo_func_t) Door_Open;
    command_if_ptr->cmd_name = "CloseDoor";
    return command_if_ptr;
}

void DoorCloseCmd_Destroy(command_if_ptr_t command_if_ptr)
{
    if (command_if_ptr)
    {
        PLATFORM_FREE(command_if_ptr);
        command_if_ptr = NULL;
    }
}



#ifdef __cplusplus
extern "C" {
#endif

/*
 * command.h
 *
 *  Created on: 20 Mar 2017
 *      Author: Özen Özkaya
 */

#ifndef COMMAND_H_
#define COMMAND_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/


/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/
typedef void (*execute_func_t)(void* obj_ptr);
typedef void (*undo_func_t)(void* obj_ptr);
typedef void (*redo_func_t)(void* obj_ptr);

typedef struct
{
    execute_func_t execute_func;
    undo_func_t undo_func;
    redo_func_t redo_func;
    char* cmd_name;
}command_if_t,*command_if_ptr_t;

  
 
/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* COMMAND_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"
 
#endif

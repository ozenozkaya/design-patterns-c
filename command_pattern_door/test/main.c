/*
 * main.c
 *
 *  Created on: 22 ?ub 2017
 *      Author: Özen Özkaya
 */

#include "simple_door_factory.h"
#include "command.h"


/*All of these extern opearations are just because of header file lazyness.
 * On a parallel universe I created header files for the sources,
 * but I just don't want to do it twice...*/
extern command_if_ptr_t DoorCloseCmd_Create();
extern void DoorCloseCmd_Destroy(command_if_ptr_t command_if_ptr);

extern command_if_ptr_t DoorOpenCmd_Create();
extern void DoorOpenCmd_Destroy(command_if_ptr_t command_if_ptr);

extern void RemoteControl_SubmitCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr);
extern void RemoteControl_UndoCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr);
extern void RemoteControl_RedoCommand(command_if_ptr_t command_if_ptr, void* target_obj_ptr);

int main()
{
    /** This code creates a door factory to create doors. This is creationally
     * simple factory pattern implementation.*/
    door_factory_if_ptr_t door_factory_if_ptr = DoorFactory_CreateFactory();
    /** Command pattern implements Command classes seperately. Here we have two
     * differend command object to close and open doors.*/
    command_if_ptr_t open_command_if_ptr  = DoorOpenCmd_Create();
    command_if_ptr_t close_command_if_ptr  = DoorCloseCmd_Create();

    door_ptr_t my_wooden_door = door_factory_if_ptr->create_door_func(WOODEN_DOOR,80.0,180.0);

    /** Here RemoteControl system is the invoker and door is the client. */
    /** We submit the open command to wooden door. So the wooden door is opened.*/
    RemoteControl_SubmitCommand(open_command_if_ptr,(void*)my_wooden_door);
    /** We submit the undo open command to wooden door. So the wooden door is closed again.*/
    RemoteControl_UndoCommand(open_command_if_ptr, (void*)my_wooden_door);
    /** We again submit the open command to wooden door. So the wooden door is opened.*/
    RemoteControl_SubmitCommand(open_command_if_ptr,(void*)my_wooden_door);
    /** Then we submit the close command to wooden door. So the wooden door is closed.*/
    RemoteControl_SubmitCommand(close_command_if_ptr,(void*)my_wooden_door);
    /** We again submit the close command to wooden door. So the wooden door is closed again.*/
    RemoteControl_RedoCommand(close_command_if_ptr,(void*)my_wooden_door);

    /** We don't need doors anymore*/
    door_factory_if_ptr->destroy_door_func(my_wooden_door);

    /** As there is no door left, we don't need door commands.*/
    DoorOpenCmd_Destroy(open_command_if_ptr);
    DoorCloseCmd_Destroy(close_command_if_ptr);
    /** Destroy the factory also, just for fun.*/
    DoorFactory_DestroyFactory(door_factory_if_ptr);

    return 0;
}

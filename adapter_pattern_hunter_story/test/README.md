## ADAPTER TEST APP

### How to build

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "src\\wild_dog.o" "..\\src\\wild_dog.c" 

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "src\\wild_dog_adapter.o" "..\\src\\wild_dog_adapter.c" 

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "src\\africa_lion.o" "..\\src\\africa_lion.c" 

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "src\\hunter.o" "..\\src\\hunter.c" 

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "src\\asia_lion.o" "..\\src\\asia_lion.c" 

gcc -std=c90 "-ID:\\workspace.despat\\adapter_pattern_dog_and_lion\\inc" -O0 -g3 -p -pg -ftest-coverage -fprofile-arcs -pedantic -pedantic-errors -Wall -Wextra -Werror -c -fmessage-length=0 -o "test\\apptest.o" "..\\test\\apptest.c" 

gcc -p -pg -ftest-coverage -fprofile-arcs -o adapter_pattern_dog_and_lion.exe "src\\africa_lion.o" "src\\asia_lion.o" "src\\hunter.o" "src\\wild_dog.o" "src\\wild_dog_adapter.o" "test\\apptest.o" 



### Function output
I hunt it and it said:

Roar!!!! 

I hunt it and it said:

Bark!!!

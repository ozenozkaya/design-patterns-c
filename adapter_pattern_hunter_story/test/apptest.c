/*
 ============================================================================
 Name        : adapter_pattern_dog_and_lion.c
 Author      : Özen Özkaya
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "lion_interface.h"
#include "dog_interface.h"
#include "hunter_interface.h"

extern hunter_if_ptr_t Hunter_Create();
extern void Hunter_Destroy(hunter_if_ptr_t hunter_if_ptr);

extern lion_if_ptr_t AfricaLion_Create();
extern void AfricaLion_Destroy(lion_if_ptr_t lion_if_ptr);

extern dog_if_ptr_t WildDog_Create();
void WildDog_Destroy(dog_if_ptr_t dog_if_ptr);

extern lion_if_ptr_t WildDog_LionAdapter_Create(dog_if_ptr_t dog_if_ptr);
extern void WildDog_LionAdapter_Destroy(lion_if_ptr_t lion_if_ptr);

/*This main story is completely fictional. It tries to explain adapter pattern.*/
int main(void)
{
    /** Once upon a time, there was a cruel hunter, which only
      hunts bravest lions. */
    hunter_if_ptr_t hunter_if_ptr = Hunter_Create();

    {
        /** In a hot summer day in Africa, an Africa lion appeared*/
        lion_if_ptr_t lion_if_ptr = AfricaLion_Create();
        /** Hunter professionally hunted the brave lion
         * And in the hunt, brave lion is roared.*/
        hunter_if_ptr->hunt_func(lion_if_ptr);
        /** At the end, brave lion is hunt...*/
        AfricaLion_Destroy(lion_if_ptr);
    }

    {
        /** In another interesting day, hunter was walking around.
         * Suddenly a wild dog is appeared.*/
        dog_if_ptr_t dog_if_ptr= WildDog_Create();
        /** But brave hunter can only hunt brave lions. No problem, a wild dog - lion
         * adapter suddenly appeared! With this adapter, now even the laziest wild dogs
         * can behave like the bravest lions.*/
        lion_if_ptr_t doglion_if_ptr = WildDog_LionAdapter_Create(dog_if_ptr);
        /** By using the adapter interface, cruel hunter hunted the wild dog*/
        hunter_if_ptr->hunt_func(doglion_if_ptr);
        /** Wild dog is suddenly disappeared*/
        WildDog_Destroy(dog_if_ptr);
        /** Wild dog adapter also...*/
        WildDog_LionAdapter_Destroy(doglion_if_ptr);
    }

    /** After all successful hunts, cruel hunter is also disappeared in miser*/
    Hunter_Destroy(hunter_if_ptr);
    return EXIT_SUCCESS;
}

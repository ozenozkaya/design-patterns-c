# ADAPTER DESIGN PATTERN 
In software engineering, the adapter pattern is a software design pattern (also known as Wrapper, an alternative naming shared with the Decorator pattern) that allows the interface of an existing class to be used as another interface.[1] It is often used to make existing classes work with others without modifying their source code. ["Wiki"]

This example application tells the story below: (Full story and the call graph can be found in doxygen documentation.)

======================== CRUEL HUNTER AND LION ADAPTER =====================================================
Once upon a time, there was a cruel hunter, which only hunts bravest lions.

In a hot summer day in Africa, an Africa lion appeared

Hunter professionally hunted the brave lion And in the hunt, brave lion is roared.

At the end, brave lion is hunt...

In another interesting day, hunter was walking around. Suddenly a wild dog is appeared.

But brave hunter can only hunt brave lions. No problem, a wild dog - lion adapter suddenly appeared! 
With this adapter, now even the laziest wild dogs can behave like the bravest lions.

By using the adapter interface, cruel hunter hunted the wild dog

Wild dog is suddenly disappeared

Wild dog adapter also...

After all successful hunts, cruel hunter is also disappeared in miser 

=============================================================================================================
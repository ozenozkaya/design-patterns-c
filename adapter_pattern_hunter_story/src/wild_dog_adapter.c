/*
 * asia_lion.c
 *
 *  Created on: 8 Mar 2017
 *      Author: Özen Özkaya
 */
#define _ASIA_LION_C

#include "lion_interface.h"
#include "dog_interface.h"
#include "platform.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>

lion_if_ptr_t WildDog_LionAdapter_Create(dog_if_ptr_t dog_if_ptr)
{
    lion_if_ptr_t lion_if_ptr = PLATFORM_MALLOC(sizeof * lion_if_ptr);
    memset(lion_if_ptr,0,sizeof *lion_if_ptr);
    lion_if_ptr->roar_func = (roar_func_t)dog_if_ptr->bark_func;
    return lion_if_ptr;
}

void WildDog_LionAdapter_Destroy(lion_if_ptr_t lion_if_ptr)
{
    memset(lion_if_ptr,0,sizeof *lion_if_ptr);
    PLATFORM_FREE(lion_if_ptr);
    lion_if_ptr=NULL;
}


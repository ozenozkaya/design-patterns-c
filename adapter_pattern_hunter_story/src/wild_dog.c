/*
 * africa_lion.c
 *
 *  Created on: 8 Mar 2017
 *      Author: Özen Özkaya
 */
#define _WILD_DOG_C

#include "dog_interface.h"
#include "platform.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>

static void WildDog_Bark();

dog_if_ptr_t WildDog_Create()
{
    dog_if_ptr_t dog_if_ptr = PLATFORM_MALLOC(sizeof * dog_if_ptr);
    memset(dog_if_ptr,0,sizeof *dog_if_ptr);
    dog_if_ptr->bark_func = WildDog_Bark;
    return dog_if_ptr;
}

void WildDog_Destroy(dog_if_ptr_t dog_if_ptr)
{
    memset(dog_if_ptr,0,sizeof *dog_if_ptr);
    PLATFORM_FREE(dog_if_ptr);
    dog_if_ptr=NULL;
}

static void WildDog_Bark()
{
    printf("Bark!!!\r\n");
}


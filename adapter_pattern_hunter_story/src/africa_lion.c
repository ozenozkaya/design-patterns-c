/*
 * africa_lion.c
 *
 *  Created on: 8 Mar 2017
 *      Author: Özen Özkaya
 */
#define _AFRICA_LION_C

#include "lion_interface.h"
#include "platform.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>

static void AfricaLion_Roar(uint8_t repetition_cnt);

lion_if_ptr_t AfricaLion_Create()
{
    lion_if_ptr_t lion_if_ptr = PLATFORM_MALLOC(sizeof * lion_if_ptr);
    memset(lion_if_ptr,0,sizeof *lion_if_ptr);
    lion_if_ptr->roar_func = AfricaLion_Roar;
    return lion_if_ptr;
}

void AfricaLion_Destroy(lion_if_ptr_t lion_if_ptr)
{
    memset(lion_if_ptr,0,sizeof *lion_if_ptr);
    PLATFORM_FREE(lion_if_ptr);
    lion_if_ptr=NULL;
}

static void AfricaLion_Roar(uint8_t repetition_cnt)
{
    uint8_t i=0;
    for(i=0;i<repetition_cnt;i++)
    {
        printf("Roar!!!! ");
    }
    printf("\r\n");
}


/*
 * africa_lion.c
 *
 *  Created on: 8 Mar 2017
 *      Author: Özen Özkaya
 */
#define _HUNTER_C

#include "hunter_interface.h"
#include "lion_interface.h"
#include "platform.h"

#include <stdio.h>
#include <stddef.h>
#include <string.h>

static void Hunter_Hunt(lion_if_ptr_t lion_if_ptr);

hunter_if_ptr_t Hunter_Create()
{
    hunter_if_ptr_t hunter_if_ptr = PLATFORM_MALLOC(sizeof * hunter_if_ptr);
    memset(hunter_if_ptr,0,sizeof *hunter_if_ptr);
    hunter_if_ptr->hunt_func = Hunter_Hunt;
    return hunter_if_ptr;
}

void Hunter_Destroy(hunter_if_ptr_t hunter_if_ptr)
{
    memset(hunter_if_ptr,0,sizeof *hunter_if_ptr);
    PLATFORM_FREE(hunter_if_ptr);
    hunter_if_ptr=NULL;
}

static void Hunter_Hunt(lion_if_ptr_t lion_if_ptr)
{
    printf("I hunt it and it said:\r\n");
    lion_if_ptr->roar_func(1);
}


#ifdef __cplusplus
extern "C"
{
#endif

#ifndef INC_LION_INTERFACE_H_
#define INC_LION_INTERFACE_H_
/****************************************************************************/
/**                                                                        **/
/**                              MODULES USED                              **/
/**                                                                        **/
/****************************************************************************/

#include <stdint.h>

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC DEFINITIONS AND MACROS                     **/
/**                                                                        **/
/****************************************************************************/
#define VERSION (1.0f)

/****************************************************************************/
/**                                                                        **/
/**                      PUBLIC TYPEDEFS AND STRUCTURES                    **/
/**                                                                        **/
/****************************************************************************/


typedef void (*roar_func_t)(uint8_t repetition_cnt);

typedef struct _lion_if_t
{
    roar_func_t roar_func;
} lion_if_t, *lion_if_ptr_t;

/****************************************************************************/
/**                                                                        **/
/**                       PUBLIC/EXPORTED FUNCTIONS                        **/
/**                                                                        **/
/****************************************************************************/

#endif /* INC_LION_INTERFACE_H_ */

#ifdef __cplusplus
} // closing brace for extern "C"

#endif
